INSERT INTO rental_apps ("id", "android_discovery_url", "ios_discovery_url", "created_at", "updated_at")
    VALUES
    ('012eb469-6d60-4ad4-9249-ccbe7a19f102', 'https://example.com/cz.rekola.app/station', 'https://example.com/cz/app/rekola/id888759232/station', NOW(), NOW()),
    ('3b1be791-dea5-46be-bc91-debf49b4e97a', 'https://example.com/cz.rekola.app/bike', 'https://example.com/cz/app/rekola/id888759232/bike', NOW(), NOW());

INSERT INTO station_information (id, system_id, name, short_name, point, capacity, address, post_code, cross_street, region_id, rental_methods, is_virtual_station, station_area, vehicle_capacity, vehicle_type_capacity, is_valet_station, rental_app_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES
('rekola-4', 'd727bb19-9755-40b2-9615-01fbb6180b8b', 'Libeňský ostrov', NULL, '0101000020E61000003EA026D10FEC2C40DE5021ECEE0D4940', NULL, NULL, NULL, NULL, NULL, NULL, true, NULL, NULL, NULL, NULL, '012eb469-6d60-4ad4-9249-ccbe7a19f102', NULL, '2022-02-07 00:01:17.74+00', NULL, NULL, NOW(), NULL),
('rekola-5', 'd727bb19-9755-40b2-9615-01fbb6180b8b', 'Podolí', NULL, '0101000020E61000003627177166D52C40CA945B869B064940', NULL, NULL, NULL, NULL, NULL, NULL, true, NULL, NULL, NULL, NULL, NULL, NULL, '2022-02-07 00:01:17.741+00', NULL, NULL, NOW(), NULL);

WITH init_values AS (
    SELECT CAST(current_date - INTERVAL '1 day' + INTERVAL '10 hour' AS TIMESTAMPTZ) AT TIME ZONE 'Europe/Prague' AS start_datetime
)
INSERT INTO station_status (station_id, num_bikes_available, num_bikes_disabled, num_docks_available, is_installed, is_renting, is_returning, last_reported, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES
('rekola-4', 1, NULL, NULL, true, true, true, (SELECT start_datetime FROM init_values), NULL, '2022-02-07 00:01:17.759+00', NULL, NULL, '2022-02-07 00:01:17.759+00', NULL),
('rekola-5', 3, NULL, NULL, true, true, true, (SELECT start_datetime FROM init_values), NULL, '2022-02-07 00:01:17.764+00', NULL, NULL, '2022-02-07 00:01:17.764+00', NULL);

INSERT INTO vehicle_types (id, form_factor, propulsion_type, max_range_meters, name, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES
('rekola-bike', 'bicycle', 'human', NULL, 'bicycle', NULL, '2022-02-07 00:01:17.721+00', NULL, NULL, '2022-02-07 00:01:17.721+00', NULL);

INSERT INTO station_status_vehicle_type (station_id, vehicle_type_id, count, num_bikes_available, num_bikes_disabled, vehicle_docks_available, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES
('rekola-4', 'rekola-bike', 1, 1, NULL, NULL, NULL, '2022-02-07 00:01:17.783+00', NULL, NULL, '2022-02-07 00:01:17.783+00', NULL),
('rekola-5', 'rekola-bike', 3, 3, NULL, NULL, NULL, '2022-02-07 00:01:17.783+00', NULL, NULL, '2022-02-07 00:01:17.783+00', NULL);

WITH init_values AS (
    SELECT CAST(current_date - INTERVAL '1 day' + INTERVAL '10 hour' AS TIMESTAMPTZ) AT TIME ZONE 'Europe/Prague' AS start_datetime
)
INSERT INTO bike_status (id, system_id, point, helmets, passengers, damage_description, description, vehicle_registration, is_reserved, is_disabled, vehicle_type_id, last_reported, current_range_meters, charge_percent, rental_app_id, station_id, create_batch_id, created_at, created_by, update_batch_id, updated_at, updated_by)
VALUES
('rekola-1926', 'd727bb19-9755-40b2-9615-01fbb6180b8b', '0101000020E61000003EA026D10FEC2C40DE5021ECEE0D4940', NULL, NULL, NULL, '', NULL, false, false, 'rekola-bike', (SELECT start_datetime FROM init_values), NULL, NULL, '3b1be791-dea5-46be-bc91-debf49b4e97a', 'rekola-4', NULL, '2022-02-07 00:01:17.759+00', NULL, NULL, NOW(), NULL),
('rekola-3206', 'd727bb19-9755-40b2-9615-01fbb6180b8b', '0101000020E61000003627177166D52C40CA945B869B064940', NULL, NULL, NULL, '', NULL, false, false, 'rekola-bike', (SELECT start_datetime FROM init_values), NULL, NULL, '3b1be791-dea5-46be-bc91-debf49b4e97a', 'rekola-5', NULL, '2022-02-07 00:01:17.765+00', NULL, NULL, NOW(), NULL),
('rekola-6968', 'd727bb19-9755-40b2-9615-01fbb6180b8b', '0101000020E61000003627177166D52C40CA945B869B064940', NULL, NULL, NULL, '', NULL, false, false, 'rekola-bike', (SELECT start_datetime FROM init_values), NULL, NULL, '3b1be791-dea5-46be-bc91-debf49b4e97a', 'rekola-5', NULL, '2022-02-07 00:01:17.765+00', NULL, NULL, NOW(), NULL),
('rekola-7310', 'd727bb19-9755-40b2-9615-01fbb6180b8b', '0101000020E61000003627177166D52C40CA945B869B064940', NULL, NULL, NULL, '', NULL, false, false, 'rekola-bike', (SELECT start_datetime FROM init_values), NULL, NULL, NULL, 'rekola-5', NULL, '2022-02-07 00:01:17.765+00', NULL, NULL, NOW(), NULL),
('rekola-1008', 'd727bb19-9755-40b2-9615-01fbb6180b8b', '0101000020E610000003FD3B23D5D82C40409EB8C047094940', NULL, NULL, NULL, '', NULL, false, false, 'rekola-bike', (SELECT start_datetime FROM init_values), NULL, NULL, NULL, NULL, NULL, '2022-02-07 00:01:17.801+00', NULL, NULL, NOW(), NULL),
('rekola-1033', 'd727bb19-9755-40b2-9615-01fbb6180b8b', '0101000020E6100000892B7E4263E32C40290B045D990B4940', NULL, NULL, NULL, '', NULL, false, false, 'rekola-bike', (SELECT start_datetime FROM init_values), NULL, NULL, NULL, NULL, NULL, '2022-02-07 00:01:17.801+00', NULL, NULL, NOW(), NULL);
