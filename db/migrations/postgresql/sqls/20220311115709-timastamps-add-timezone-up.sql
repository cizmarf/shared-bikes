ALTER TABLE "station_status"
    ALTER COLUMN "last_reported" TYPE TIMESTAMP WITH TIME ZONE USING "last_reported" AT TIME ZONE 'UTC';

ALTER TABLE "bike_status"
    ALTER COLUMN "last_reported" TYPE TIMESTAMP WITH TIME ZONE USING "last_reported" AT TIME ZONE 'UTC';

ALTER TABLE "geofencing_zones"
    ALTER COLUMN "start" TYPE TIMESTAMP WITH TIME ZONE USING "start" AT TIME ZONE 'UTC';

ALTER TABLE "geofencing_zones"
    ALTER COLUMN "end" TYPE TIMESTAMP WITH TIME ZONE USING "end" AT TIME ZONE 'UTC';

ALTER TABLE "system_information"
    ALTER COLUMN "start_date" TYPE TIMESTAMP WITH TIME ZONE USING "start_date" AT TIME ZONE 'UTC';

ALTER TABLE "pricing_plans"
    ALTER COLUMN "last_updated" TYPE TIMESTAMP WITH TIME ZONE USING "last_updated" AT TIME ZONE 'UTC';
