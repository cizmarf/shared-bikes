TRUNCATE bike_status;
ALTER TABLE bike_status ALTER COLUMN rental_app_id SET NOT NULL;

TRUNCATE station_information CASCADE;
ALTER TABLE station_information DROP CONSTRAINT station_information_system_id_fkey;
ALTER TABLE station_information DROP COLUMN system_id;

INSERT INTO rental_apps ("id", "android_store_url", "android_discovery_url", "ios_store_url", "ios_discovery_url", "web_url", "created_at", "updated_at")
    VALUES ('bb622abc-84a1-11ec-a8a3-0242ac120002', 'https://play.google.com/store/apps/details?id=cz.rekola.app&hl=cs', 'https://play.google.com/store/apps/details?id=cz.rekola.app&hl=cs', 'https://itunes.apple.com/cz/app/rekola/id888759232?mt=8', 'https://itunes.apple.com/cz/app/rekola/id888759232?mt=8', 'https://www.rekola.cz', NOW(), NOW())
    ON CONFLICT (id) DO NOTHING;
