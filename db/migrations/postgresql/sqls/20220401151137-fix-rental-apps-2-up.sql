DROP TABLE IF EXISTS rental_apps_system_info;

ALTER TABLE system_information ADD COLUMN rental_app_id character varying(50) COLLATE pg_catalog."default";
ALTER TABLE system_information ADD CONSTRAINT system_information_rental_app_id_fkey FOREIGN KEY (rental_app_id) REFERENCES rental_apps(id);

INSERT INTO rental_apps ("id", "android_store_url", "android_discovery_url", "ios_store_url", "ios_discovery_url", "web_url", "created_at", "updated_at")
    VALUES ('bb622abc-84a1-11ec-a8a3-0242ac120002', 'https://play.google.com/store/apps/details?id=cz.rekola.app&hl=cs', 'https://play.google.com/store/apps/details?id=cz.rekola.app&hl=cs', 'https://itunes.apple.com/cz/app/rekola/id888759232?mt=8', 'https://itunes.apple.com/cz/app/rekola/id888759232?mt=8', 'https://www.rekola.cz', NOW(), NOW())
    ON CONFLICT (id) DO NOTHING;

UPDATE system_information
    SET rental_app_id='bb622abc-84a1-11ec-a8a3-0242ac120002'
    WHERE system_id = 'd727bb19-9755-40b2-9615-01fbb6180b8b';
