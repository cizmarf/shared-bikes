-- Static data for Rekola
UPDATE pricings SET ("pricing_type", "rate", "interval") = (NULL, 24/30, 0)
    WHERE pricing_plan_id = '4b72f960-84cf-11ec-a8a3-0242ac120002';

UPDATE system_information SET "license_url" = NULL
    WHERE system_id = 'd727bb19-9755-40b2-9615-01fbb6180b8b';

-- END

ALTER TABLE station_status_vehicle_type RENAME COLUMN "vehicle_docks_available" TO "num_docks_available";
ALTER TABLE station_status_vehicle_type DROP COLUMN "count";

ALTER TABLE station_information DROP COLUMN "capacity";

ALTER TABLE system_information DROP COLUMN "terms_of_use_url";

DROP TABLE pricing_plan_payment;
DROP TABLE geofencing_zones;

CREATE TABLE IF NOT EXISTS geofencing_zones (
    "id" text,
    "system_id" varchar(50) NOT NULL,
    "name" text,
    "start" timestamp,
    "end" timestamp,
    "geom" geometry NOT NULL,

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT geofencing_zones_pkey PRIMARY KEY (id),
    CONSTRAINT geofencing_zones_system_id_fkey FOREIGN KEY (system_id) REFERENCES system_information(system_id)
);

CREATE TABLE IF NOT EXISTS geofencing_rules (
    "geofencing_zone_id" text NOT NULL,
    "ride_allowed" boolean NOT NULL,
    "ride_through_allowed" boolean NOT NULL,
    "maximum_speed_kph" integer,
    "parking_allowed" boolean NOT NULL,

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT geofencing_rules_zone_id_fkey FOREIGN KEY (geofencing_zone_id) REFERENCES geofencing_zones(id)
        ON DELETE CASCADE
);
