ALTER TABLE system_information DROP CONSTRAINT system_information_rental_app_id_fkey;
ALTER TABLE system_information DROP COLUMN rental_app_id;

TRUNCATE rental_apps CASCADE;

CREATE TABLE IF NOT EXISTS rental_apps_system_info (
    "rental_app_id" varchar(50) NOT NULL,
    "system_id" varchar(50) NOT NULL,

    -- Audit fields
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by character varying(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by character varying(150),

    CONSTRAINT rental_apps_system_info_rental_app_id_fkey FOREIGN KEY (rental_app_id) REFERENCES rental_apps(id),
    CONSTRAINT rental_apps_system_info_system_id_fkey FOREIGN KEY (system_id) REFERENCES system_information(system_id)
);
