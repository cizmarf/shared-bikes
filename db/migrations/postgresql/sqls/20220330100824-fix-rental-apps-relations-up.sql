TRUNCATE bike_status;
ALTER TABLE bike_status ALTER COLUMN rental_app_id DROP NOT NULL;

TRUNCATE station_information CASCADE;
ALTER TABLE station_information ADD COLUMN system_id character varying(50) COLLATE pg_catalog."default" NOT NULL;
ALTER TABLE station_information ADD CONSTRAINT station_information_system_id_fkey FOREIGN KEY (system_id) REFERENCES system_information(system_id);

TRUNCATE rental_apps CASCADE;
