import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { NextbikeSystemInformationTransformation } from "#ie/transformations";
import { INextbikeSystemInformationInput } from "#sch/datasources";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";

chai.use(chaiAsPromised);

describe("NextbikeSystemInformationTransformation", () => {
    let transformation: NextbikeSystemInformationTransformation;
    let testSourceData: INextbikeSystemInformationInput;

    beforeEach(() => {
        transformation = new NextbikeSystemInformationTransformation("TestId");
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-system-information-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeSystemInfoDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data.systemInfo).to.deep.equal(nextbikeTransformedDataFixture.systemInformation[0]);
        expect(nextbikeTransformedDataFixture.rentalApps).to.include.deep.members([data.rentalApps]);
    });
});
