import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { NextbikeFreeBikeStatusTransformation } from "#ie/transformations";
import { INextbikeFreeBikeStatusInput } from "#sch/datasources";
import { nextbikeTransformedDataFixture } from "../../data/nextbikeTransformedData.fixture";

chai.use(chaiAsPromised);

describe("NextbikeFreeBikeStatusTransformation", () => {
    let transformation: NextbikeFreeBikeStatusTransformation;
    let testSourceData: INextbikeFreeBikeStatusInput;

    beforeEach(() => {
        transformation = new NextbikeFreeBikeStatusTransformation("TestId");
        testSourceData = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/nextbike-free-bike-status-datasource.json").toString("utf8")
        );
    });

    it("should have name", () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesNextbikeFreeBikeStatusDataSourceTestIdTransformation");
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data.bikeStatus).to.deep.equal(nextbikeTransformedDataFixture.bikeStatus);
        expect(nextbikeTransformedDataFixture.rentalApps).to.include.deep.members(data.rentalApps);
    });
});
