import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { systemIdFixture } from "../data/BeRiderSystemInformation.fixture";
import { BeRiderVehiclesTransformation } from "#ie/transformations";
import { BeRiderTransformedVehiclesFixture } from "../data/BeRiderTransformedVehicleData.fixture";

chai.use(chaiAsPromised);

describe("BeRider vehicles transformation", () => {
    let transformationDate = new Date("2022-04-10T09:21:00.000Z");
    let testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/berider_vehicles-datasource.json").toString("utf8"));

    let transformation = new BeRiderVehiclesTransformation("BeRiderVehiclesDataSource", systemIdFixture, transformationDate);

    it("should properly transform data", async () => {
        const data = await transformation.transform({ vehicles: testSourceData });
        expect(data).to.deep.equal(BeRiderTransformedVehiclesFixture);
    });
});
