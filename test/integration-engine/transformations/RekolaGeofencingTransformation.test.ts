import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { RekolaGeofencingTransformation } from "#ie/transformations/RekolaGeofencingTransformation";

chai.use(chaiAsPromised);

describe("RekolaGeofencingTransformation", () => {
    let transformation: RekolaGeofencingTransformation;
    let testSourceData: any;
    let testTransformedData: any;

    beforeEach(async () => {
        transformation = new RekolaGeofencingTransformation("rekola-", "d727bb19-9755-40b2-9615-01fbb6180b8b");
        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/rekola_zones-datasource.json").toString("utf8"));
        testTransformedData = JSON.parse(fs.readFileSync(__dirname + "/../data/rekola_zones-transformed.json").toString("utf8"));
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesRekolaGeofencingZonesDataSource");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform data", async () => {
        const { zones } = await transformation.transform({
            items: testSourceData,
        });
        for (let i = 0, imax = zones.length; i < imax; i++) {
            expect(zones[i]).to.deep.equal(testTransformedData[i]);
        }
    });
});
