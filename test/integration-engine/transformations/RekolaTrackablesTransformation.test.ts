import sinon, { SinonSandbox } from "sinon";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { RekolaTrackablesTransformation } from "#ie/transformations/RekolaTrackablesTransformation";

chai.use(chaiAsPromised);

describe("RekolaTrackablesTransformation", () => {
    let sandbox: SinonSandbox;
    let transformation: RekolaTrackablesTransformation;
    let testSourceData: any;
    let testTransformedData: any;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        sandbox.stub(Date.prototype, "toISOString").returns("2022-02-07T02:58:32.410Z");
        transformation = new RekolaTrackablesTransformation("rekola-");
        testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/rekola_trackables-datasource.json").toString("utf8"));
        testTransformedData = JSON.parse(
            fs.readFileSync(__dirname + "/../data/rekola_trackables-transformed.json").toString("utf8")
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SharedBikesRekolaTrackablesDataSource");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform data", async () => {
        const data = await transformation.transform({
            ...testSourceData,
            systemId: "d727bb19-9755-40b2-9615-01fbb6180b8b",
            bikeStatusRentalAppId: null,
            stationInformationRentalAppId: null,
        });
        expect(data).to.deep.equal(testTransformedData);
    });
});
