import { expect } from "chai";
import { BeRiderVehiclesDatasourceFixture } from "../../data/BeRiderVehiclesDatasource.fixture";
import { transformBikeStatus } from "#ie/transformations/BeRider/transformBikeStatus";
import { BeRiderTransformedVehicleFixture1 } from "../../data/BeRiderTransformedVehicleData.fixture";
import { systemIdFixture } from "../../data/BeRiderSystemInformation.fixture";

describe("BeRider transformBikeStatus", () => {
    it("transforms valid data correctly", () => {
        const currentDate = new Date("2022-04-10T09:21:00.000Z");
        const expected = BeRiderTransformedVehicleFixture1;
        const actual = transformBikeStatus(BeRiderVehiclesDatasourceFixture[0], systemIdFixture, currentDate);
        expect(actual).to.deep.equal(expected);
    });
});
