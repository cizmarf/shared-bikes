import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { systemIdFixture } from "../data/BeRiderSystemInformation.fixture";
import { BeRiderGeofencingTransformation } from "#ie/transformations";
import { BeRiderTransformedGeofencingDataFixture } from "../data/BeRiderTransformedGeofencingData.fixture";

chai.use(chaiAsPromised);

describe("BeRider geofencing transformation", () => {
    let testSourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/berider_zones-datasource.json").toString("utf8"));

    let transformation = new BeRiderGeofencingTransformation("BeRiderGeofencingZonesDataSource", systemIdFixture);

    it("should properly transform data", async () => {
        const data = await transformation.transform({ zones: testSourceData });
        expect(data).to.deep.equal(BeRiderTransformedGeofencingDataFixture);
    });
});
