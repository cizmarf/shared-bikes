import { BeRiderStaticDataService } from "#ie";
import { expect } from "chai";
import { BeRiderSystemInformationFixture, systemIdFixture } from "./data/BeRiderSystemInformation.fixture";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import sinon, { SinonSandbox } from "sinon";
import { SystemInformationModel } from "#ie/models";

describe("BeRiderStaticDataService", async () => {
    let sandbox: SinonSandbox;
    const systemInformationModel = new SystemInformationModel();
    const beRiderStaticDataService = new BeRiderStaticDataService(systemInformationModel);

    beforeEach(async () => {
        sandbox = sinon.createSandbox();
        await PostgresConnector.connect();
        await systemInformationModel.deleteBySystemId(systemIdFixture);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("saves system information correctly", async () => {
        const emptySystemInformation = await systemInformationModel.GetOne(systemIdFixture);

        expect(emptySystemInformation).to.be.null;

        await beRiderStaticDataService.saveBeRiderSystemBasics(systemIdFixture);
        const systemInformation = await systemInformationModel.GetOne(systemIdFixture);

        expect(systemInformation).to.deep.equal(BeRiderSystemInformationFixture);
    });
});
