import { ISharedBikesGeofencingZoneOutput } from "#sch";

export const BeRiderTransformedGeofencingDataFixture: ISharedBikesGeofencingZoneOutput[] = [
    {
        id: "berider-zone-32ddc9031d",
        system_id: "system-id-fixture-guid",
        name: "berider-zone-32ddc9031d",
        note: null,
        source: null,
        price: null,
        priority: 0,
        start: null,
        end: null,
        geom: {
            type: "MultiPolygon",
            coordinates: [
                [
                    [
                        [14.417137, 50.087111],
                        [14.417315, 50.086678],
                        [14.417619, 50.086754],
                        [14.417137, 50.087111],
                    ],
                    [
                        [14.440852403640733, 50.084132778328055],
                        [14.441506862640367, 50.084105241171855],
                        [14.437934160232528, 50.07940303978436],
                        [14.440004825592041, 50.081929755842694],
                        [14.440852403640733, 50.084132778328055],
                    ],
                ],
                [
                    [
                        [14.392337, 50.07342],
                        [14.387798, 50.072524],
                        [14.384547, 50.071894],
                        [14.383678, 50.071925],
                        [14.392337, 50.07342],
                    ],
                    [
                        [14.403683692216871, 50.073596940378735],
                        [14.403828531503677, 50.07360382617835],
                        [14.403847306966782, 50.07317260106869],
                        [14.403718560934065, 50.07317174033604],
                        [14.403683692216871, 50.073596940378735],
                    ],
                ],
                [
                    [
                        [14.421762, 50.099741],
                        [14.422619, 50.097996],
                        [14.42257, 50.096351],
                        [14.421762, 50.099741],
                    ],
                ],
            ],
        },
        ride_allowed: true,
        ride_through_allowed: true,
        maximum_speed_kph: null,
        parking_allowed: true,
    },
];
