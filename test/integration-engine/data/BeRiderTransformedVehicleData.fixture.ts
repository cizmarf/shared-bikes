import { systemIdFixture } from "./BeRiderSystemInformation.fixture";

export const BeRiderTransformedVehicleFixture1 = {
    id: "berider-20178",
    system_id: systemIdFixture,
    point: {
        type: "Point",
        coordinates: [14.413966, 50.080049],
    },
    helmets: 2,
    passengers: 2,
    damage_description: "*  \nNemá držák na mobil / No phone holder\nŠkrábance zrcátko L+P",
    description: "Masarykovo nábřeží, 1648/36 (Praha)",
    vehicle_registration: "1A S293",
    is_reserved: false,
    is_disabled: false,
    vehicle_type_id: "berider-moped",
    last_reported: "2022-04-10T09:21:00.000Z",
    current_range_meters: 65000.0,
    charge_percent: 100,
    rental_app_id: null,
    station_id: null,
};

export const BeRiderTransformedFixture2 = {
    id: "berider-20133",
    system_id: systemIdFixture,
    point: {
        type: "Point",
        coordinates: [14.435241, 50.02495],
    },
    helmets: 2,
    passengers: 2,
    damage_description: "* Nemá stojan na mobil / No phone holder\nOdřené L+P zrcátko",
    description: "Novodvorská",
    vehicle_registration: "1A P358",
    is_reserved: false,
    is_disabled: false,
    vehicle_type_id: "berider-moped",
    last_reported: "2022-04-10T09:21:00.000Z",
    current_range_meters: 26650.0,
    charge_percent: 41,
    rental_app_id: null,
    station_id: null,
};

export const BeRiderTransformedVehiclesFixture = [BeRiderTransformedVehicleFixture1, BeRiderTransformedFixture2];
