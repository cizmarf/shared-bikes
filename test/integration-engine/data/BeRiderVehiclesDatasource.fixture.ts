import { IBeRiderVehicle } from "#sch/datasources/BeRiderVehicle";

const BeRiderVehicleFixture1: IBeRiderVehicle = {
    id: 20178,
    lat: 50.080049,
    lon: 14.413966,
    vehicleRegistration: "1A S293",
    charge: 100,
    stateId: 0,
    typeId: 3,
    reservationStateId: 0,
    address: "Masarykovo nábřeží, 1648/36 (Praha)",
    zipCode: "11000",
    city: "Praha",
    helmets: 2,
    hardwareId: "357520073929735",
    damageDescription: "*  \nNemá držák na mobil / No phone holder\nŠkrábance zrcátko L+P",
    fuelCardPin: null,
    estimatedRange: 65000.0,
    isHelmetDetectionSupported: false,
    isClean: false,
    isDamaged: true,
    isRequestRunning: false,
};

const BeRiderVehicleFixture2: IBeRiderVehicle = {
    id: 20133,
    lat: 50.02495,
    lon: 14.435241,
    vehicleRegistration: "1A P358",
    charge: 41,
    stateId: 0,
    typeId: 3,
    reservationStateId: 0,
    address: "Novodvorská",
    zipCode: "",
    city: "",
    helmets: 2,
    hardwareId: "357520072879857",
    damageDescription: "* Nemá stojan na mobil / No phone holder\nOdřené L+P zrcátko",
    fuelCardPin: null,
    estimatedRange: 26650.0,
    isHelmetDetectionSupported: false,
    isClean: false,
    isDamaged: true,
    isRequestRunning: false,
};

export const BeRiderVehiclesDatasourceFixture = [BeRiderVehicleFixture1, BeRiderVehicleFixture2];
