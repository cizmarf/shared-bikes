export const systemIdFixture = "system-id-fixture-guid";

export const BeRiderSystemInformationFixture = {
    operator_id: "berider",
    system_id: systemIdFixture,
    language: "cs",
    logo: "https://bike-rent.be-rider.com/static/eeb9a3d9a56c12a7e20f2510bf263589/d8e72/berider-logo-black.png",
    name: "Sdílené elektrické skútry BeRider",
    short_name: "BeRider",
    operator: null,
    url: null,
    purchase_url: "https://www.be-rider.com/download",
    start_date: new Date("2022-04-29 12:00:00.000000+00:00"),
    phone_number: "+420 720 627 505",
    email: "info@be-rider.com",
    feed_contact_email: "info@be-rider.com",
    timezone: "Europe/Prague",
    license_id: null,
    license_url: "https://s3-prague.stor.geetoo.com/rabin-ipt/Legal/GBFSLicence.txt",
    attribution_organization_name: null,
    attribution_url: null,
    terms_of_use_url:
        "https://global-uploads.webflow.com/5cc70992ad143a2786044c6a/5e566d96fd8ddf1ec18ed858_200226_BeRider - Price_list_web_v1-2_CZ.pdf",
    rental_app_id: "dc601405-2333-49b5-9ffe-8f76ddc09ed7",
};
