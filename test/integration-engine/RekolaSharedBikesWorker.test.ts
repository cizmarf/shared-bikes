import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RekolaSharedBikesWorker } from "#ie/RekolaSharedBikesWorker";

describe("RekolaSharedBikesWorker", () => {
    let worker: RekolaSharedBikesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
                query: sandbox.stub().returns([[{}]]),
            })
        );

        worker = new RekolaSharedBikesWorker();

        sandbox.stub(worker["zonesDatasource"], "getAll");
        sandbox.stub(worker["trackablesDatasource"], "getAll");

        sandbox.stub(worker["geofencingTransformation"], "transform").callsFake(() => Promise.resolve([] as any));
        sandbox.stub(worker["trackablesTransformation"], "transform").callsFake(() => Promise.resolve([] as any));

        sandbox.stub(worker["geofencingZonesModel"], "replace");
        sandbox.stub(worker["vehicleTypesModel"], "save");
        sandbox.stub(worker["stationInformationModel"], "createAndAssociate");
        sandbox.stub(worker["stationStatusVehicleTypesModel"], "save");
        sandbox.stub(worker["bikeStatusModel"], "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("refreshRekolaGeofencingData should call the correct methods", async () => {
        await worker.refreshRekolaGeofencingData();
        sandbox.assert.calledOnce(worker["zonesDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["geofencingZonesModel"].replace as SinonSpy);
    });

    it("refreshRekolaTrackableData should call the correct methods", async () => {
        await worker.refreshRekolaTrackableData();
        sandbox.assert.calledOnce(worker["trackablesDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["trackablesTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["vehicleTypesModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["stationInformationModel"].createAndAssociate as SinonSpy);
        sandbox.assert.calledOnce(worker["stationStatusVehicleTypesModel"].save as SinonSpy);
        sandbox.assert.calledOnce(worker["bikeStatusModel"].save as SinonSpy);
    });
});
