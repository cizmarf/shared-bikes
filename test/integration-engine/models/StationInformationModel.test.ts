import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { RekolaStationInformationModel, BikeStatusModel } from "#ie/models";
import { IStationInformationElementTransformationResult } from "#ie/transformations";

chai.use(chaiAsPromised);

describe("StationInformationModel", () => {
    let stationInformationModel: RekolaStationInformationModel;
    let bikeStatusModel: BikeStatusModel;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        PostgresConnector.connect();
        stationInformationModel = new RekolaStationInformationModel();
        bikeStatusModel = new BikeStatusModel();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("createAndAssociate should save", async () => {
        const input: IStationInformationElementTransformationResult = {
            id: "rekola-test",
            system_id: "d727bb19-9755-40b2-9615-01fbb6180b8b",
            name: "Libeňský ostrov",
            point: {
                type: "Point",
                coordinates: [14.4610581741, 50.1088538325],
            },
            is_virtual_station: true,
            rental_app_id: null,
            station_status: {
                num_bikes_available: 1,
                is_installed: true,
                is_renting: true,
                is_returning: true,
                last_reported: "2020-02-07 00:01:17.74+00",
            },
            bike_status: [
                {
                    id: "rekola-test",
                    system_id: "d727bb19-9755-40b2-9615-01fbb6180b8b",
                    point: {
                        type: "Point",
                        coordinates: [14.4610581741, 50.1088538325],
                    },
                    helmets: null,
                    passengers: null,
                    damage_description: null,
                    description: "",
                    vehicle_registration: null,
                    is_reserved: false,
                    is_disabled: false,
                    vehicle_type_id: "rekola-bike",
                    last_reported: "2020-02-07 00:01:17.74+00",
                    current_range_meters: null,
                    charge_percent: null,
                    rental_app_id: null,
                    station_id: null,
                },
            ],
        };

        const data = await stationInformationModel.createAndAssociate([input]);
        const stationId = data[0].getDataValue("id");
        expect(data[0].getDataValue("station_status").getDataValue("station_id")).to.equal(stationId);
        const vehicle = await bikeStatusModel.findOne({ where: { station_id: stationId } });
        expect(vehicle.station_id).to.equal(stationId);
    });

    it("upsertAssociatedVehicles should upsert", async () => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        const vehicle = await stationInformationModel["upsertAssociatedVehicles"](
            [
                {
                    id: "rekola-test",
                    system_id: "d727bb19-9755-40b2-9615-01fbb6180b8b",
                    point: {
                        type: "Point",
                        coordinates: [14.4610581741, 50.1088538325],
                    },
                    helmets: null,
                    passengers: null,
                    damage_description: null,
                    description: "Poggers",
                    vehicle_registration: null,
                    is_reserved: false,
                    is_disabled: false,
                    vehicle_type_id: "rekola-bike",
                    last_reported: "2020-02-07 00:01:17.74+00",
                    current_range_meters: null,
                    charge_percent: null,
                    rental_app_id: null,
                    station_id: null,
                },
            ],
            "rekola-test",
            t
        );
        expect((vehicle[0][0] as any).getDataValue("id")).to.equal("rekola-test");
        expect((vehicle[0][0] as any).getDataValue("description")).to.equal("Poggers");
        await t.rollback();
    });

    it("deleteNHoursOldData should delete", async () => {
        const deletedRows = await stationInformationModel.deleteNHoursOldData(42);
        expect(deletedRows).to.eq(1);
    });
});
