import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { SharedBikesWorker } from "#ie/SharedBikesWorker";

describe("SharedBikesWorker", () => {
    let worker: SharedBikesWorker;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub(),
            })
        );

        worker = new SharedBikesWorker();

        sandbox.stub(worker["stationInformationModel"], "deleteNHoursOldData");
        sandbox.stub(worker["bikeStatusModel"], "deleteNHoursOldData");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("deleteOldTrackableData should call the correct methods", async () => {
        await worker.deleteOldTrackableData({ content: Buffer.from(JSON.stringify({ targetHours: 48 })) });
        sandbox.assert.calledOnceWithExactly(worker["stationInformationModel"].deleteNHoursOldData as SinonSpy, 48);
        sandbox.assert.calledOnceWithExactly(worker["bikeStatusModel"].deleteNHoursOldData as SinonSpy, 48);
    });
});
