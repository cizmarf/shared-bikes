import "@golemio/core/dist/shared/sequelize"; // Workaround for inferred type of forExport
import {
    rekolaGeofencingZones,
    rekolaTrackables,
    nextbikeDefinitionJsonSchema,
    nextbikeSystemInfoJsonSchema,
    nextbikeFreeBikeStatusJsonSchema,
    nextbikeStationInfoJsonSchema,
    nextbikeStationStatusJsonSchema,
    nextbikeSystemPricingPlansJsonSchema,
    BeRiderVehiclesJsonSchema,
    BeRiderGeofencingJsonSchema,
} from "./datasources";
import {
    systemInformation,
    rentalApps,
    geofencingZones,
    pricingPlans,
    pricings,
    stationInformation,
    stationStatus,
    vehicleTypes,
    stationsStatusVehicleType,
    bikeStatus,
} from "./definitions";

const nextbikeDataSources = {
    nextbikeDefinitionJsonSchema,
    nextbikeFreeBikeStatusJsonSchema,
    nextbikeStationInfoJsonSchema,
    nextbikeStationStatusJsonSchema,
    nextbikeSystemInfoJsonSchema,
    nextbikeSystemPricingPlansJsonSchema,
};

export type TNextbikeDataSources = keyof typeof nextbikeDataSources;

const forExport = {
    name: "SharedBikes",
    pgSchema: "bikesharing",
    datasources: {
        rekolaGeofencingZones,
        rekolaTrackables,
        ...nextbikeDataSources,
        BeRiderVehiclesJsonSchema,
        BeRiderGeofencingJsonSchema,
    },
    definitions: {
        systemInformation,
        rentalApps,
        geofencingZones,
        pricingPlans,
        pricings,
        stationInformation,
        stationStatus,
        vehicleTypes,
        stationsStatusVehicleType,
        bikeStatus,
    },
};

export { forExport as SharedBikes };

export type {
    IRekolaGeofencingDatasourceVehicleType,
    IRekolaGeofencingDatasourceZone,
    IRekolaGeofencingDatasourceItem,
    IRekolaTrackablesDatasourceRack,
    IRekolaTrackablesDatasourceVehicle,
    IRekolaTrackablesDatasource,
} from "./datasources";

export type {
    ISharedBikesSystemInformationOutput,
    ISharedBikesRentalAppOutput,
    ISharedBikesGeofencingZoneOutput,
    ISharedBikesPricingPlanOutput,
    ISharedBikesPricingOutput,
    ISharedBikesStationInformationOutput,
    ISharedBikesStationStatusOutput,
    ISharedBikesVehicleTypeOutput,
    ISharedBikesStationStatusVehicleTypeOutput,
    ISharedBikesBikeStatusOutput,
} from "./definitions";
