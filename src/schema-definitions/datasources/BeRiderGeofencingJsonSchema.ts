import { BasicTypes } from "#sch/datasources/shared";

const BeRiderGeofencingCoordinateSchema = {
    index: {
        type: BasicTypes.number,
        lat: BasicTypes.number,
        lon: BasicTypes.number,
    },
};

const BeRiderGeofencingPolygonSchema = {
    id: {
        type: BasicTypes.number,
    },
    coordinates: {
        type: BasicTypes.array,
        items: BeRiderGeofencingCoordinateSchema,
    },
    isCutOut: {
        type: BasicTypes.boolean,
    },
};

const BeRiderGeofencingObjectSchema = {
    id: {
        type: BasicTypes.number,
    },
    name: {
        type: BasicTypes.string,
    },
    polygons: {
        type: BasicTypes.array,
        items: BeRiderGeofencingPolygonSchema,
    },
};

export const BeRiderGeofencingJsonSchema = {
    name: "SharedBikesBeRiderGeofencingDataSource",
    jsonSchema: {
        type: "array",
        items: BeRiderGeofencingObjectSchema,
    },
};
