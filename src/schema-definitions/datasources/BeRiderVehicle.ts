export interface IBeRiderVehicle {
    id: number;
    lat: number;
    lon: number;
    vehicleRegistration: string;
    charge: number;
    stateId: number;
    typeId: number;
    reservationStateId: number;
    address: string;
    zipCode: string;
    city: string;
    helmets: number;
    hardwareId: string;
    damageDescription: string;
    fuelCardPin: null;
    estimatedRange: number;
    isHelmetDetectionSupported: boolean;
    isClean: boolean;
    isDamaged: boolean;
    isRequestRunning: boolean;
}
