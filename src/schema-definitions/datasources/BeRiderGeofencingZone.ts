export interface BeRiderGeoJsonFeatures {
    type: string;
    geometry: {
        type: string;
        coordinates: number[][][];
    };
}

export interface BeRiderGeofencingZone {
    hash: number;
    type: string;
    name: string;
    order: number;
    geoJson: {
        type: string;
        features: BeRiderGeoJsonFeatures[];
    };
}
