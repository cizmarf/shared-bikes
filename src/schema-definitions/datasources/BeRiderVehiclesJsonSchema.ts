import { BasicTypes } from "#sch/datasources/shared";

const BeRiderVehicleObjectSchema = {
    type: BasicTypes.object,
    properties: {
        id: {
            type: BasicTypes.number,
        },
        lat: {
            type: BasicTypes.number,
        },
        lon: {
            type: BasicTypes.number,
        },
        vehicleRegistration: {
            type: BasicTypes.string,
        },
        charge: {
            type: BasicTypes.number,
        },
        stateId: {
            type: BasicTypes.number,
        },
        typeId: {
            type: BasicTypes.number,
        },
        reservationStateId: {
            type: BasicTypes.number,
        },
        address: {
            type: BasicTypes.string,
        },
        zipCode: {
            type: BasicTypes.string,
        },
        city: {
            type: BasicTypes.string,
        },
        helmets: {
            type: BasicTypes.number,
        },
        hardwareId: {
            type: BasicTypes.string,
        },
        damageDescription: {
            type: BasicTypes.string,
        },
        fuelCardPin: {
            type: BasicTypes.null,
        },
        estimatedRange: {
            type: BasicTypes.number,
        },
        isHelmetDetectionSupported: {
            type: BasicTypes.boolean,
        },
        isClean: {
            type: BasicTypes.boolean,
        },
        isDamaged: {
            type: BasicTypes.boolean,
        },
        isRequestRunning: {
            type: BasicTypes.boolean,
        },
        helmetDetectionSupported: {
            type: BasicTypes.boolean,
        },
        clean: {
            type: BasicTypes.boolean,
        },
        damaged: {
            type: BasicTypes.boolean,
        },
        requestRunning: {
            type: BasicTypes.boolean,
        },
    },
};

export const BeRiderVehiclesJsonSchema = {
    name: "SharedBikesBeRiderVehiclesDataSource",
    jsonSchema: {
        type: "array",
        items: BeRiderVehicleObjectSchema,
    },
};
