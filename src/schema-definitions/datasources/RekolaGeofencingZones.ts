import { RekolaGeoPoint, geoPointJsonSchema } from "./shared";

export const rekolaGeofencingZones = {
    name: "SharedBikesRekolaGeofencingZonesDataSource",
    jsonSchema: {
        type: "array",
        items: {
            type: "object",
            properties: {
                vehicleType: {
                    type: "object",
                    properties: {
                        type: {
                            type: "string",
                        },
                        name: {
                            type: "string",
                        },
                        description: {
                            type: "string",
                        },
                        icon: {
                            type: "string",
                        },
                        color: {
                            type: "string",
                        },
                    },
                },
                boundingBox: {
                    type: "array",
                    items: geoPointJsonSchema,
                },
                zones: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            id: {
                                type: "number",
                            },
                            boundingBox: {
                                type: "array",
                                items: geoPointJsonSchema,
                            },
                            points: {
                                type: "array",
                                items: {
                                    type: "array",
                                    items: {
                                        type: "number",
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
    },
};

export interface IRekolaGeofencingDatasourceVehicleType {
    type: string;
    name: string;
    description: string;
    icon: string;
    color: string;
}

export interface IRekolaGeofencingDatasourceZone {
    id: number;
    boundingBox: [RekolaGeoPoint, RekolaGeoPoint];
    points: number[][];
}

export interface IRekolaGeofencingDatasourceItem {
    vehicleType: IRekolaGeofencingDatasourceVehicleType;
    boundingBox: [RekolaGeoPoint, RekolaGeoPoint];
    zones: IRekolaGeofencingDatasourceZone[];
}
