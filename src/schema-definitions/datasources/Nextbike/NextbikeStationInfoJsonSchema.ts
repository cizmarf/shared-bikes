import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { INextbikeRentalUris, ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";
import { NextbikeRentalUrisJsonSchema } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export interface INextbikeStationInformation {
    station_id: string;
    name: string;
    short_name: string;
    lat: number;
    lon: number;
    region_id: string;
    rental_uris: INextbikeRentalUris;
    is_virtual_station: boolean;
    capacity: number;
}

export interface INextbikeStationInformationInput {
    last_updated: number;
    ttl: number;
    version: string;
    data: {
        stations: INextbikeStationInformation[];
    };
}

const StationInfoJsonSchema: JSONSchemaType<INextbikeStationInformation> = {
    type: "object",
    properties: {
        station_id: {
            type: "string",
        },
        name: {
            type: "string",
        },
        short_name: {
            type: "string",
        },
        lat: {
            type: "number",
        },
        lon: {
            type: "number",
        },
        region_id: {
            type: "string",
        },
        rental_uris: NextbikeRentalUrisJsonSchema,
        is_virtual_station: {
            type: "boolean",
        },
        capacity: {
            type: "number",
        },
    },
    required: ["station_id", "name", "short_name", "lat", "lon", "region_id", "rental_uris", "is_virtual_station"],
    additionalProperties: false,
};

export const nextbikeStationInfoJsonSchema: ISchemaDefinition<INextbikeStationInformationInput> = {
    name: "SharedBikesNextbikeStationInfoDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            last_updated: {
                type: "number",
            },
            ttl: {
                type: "number",
            },
            version: {
                type: "string",
            },
            data: {
                type: "object",
                properties: {
                    stations: {
                        type: "array",
                        items: StationInfoJsonSchema,
                    },
                },
                required: ["stations"],
                additionalProperties: false,
            },
        },
        required: ["last_updated", "ttl", "version", "data"],
        additionalProperties: false,
    },
};
