import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { ISchemaDefinition } from "#sch/datasources/Nextbike/NextbikeSharedJsonSchema";

export interface INextbikeRentalApps {
    android: INextbikeRentalAppUris;
    ios: INextbikeRentalAppUris;
}

interface INextbikeRentalAppUris {
    store_uri: string;
    discovery_uri: string;
}

export interface INextbikeSystemInformation {
    system_id: string;
    language: string;
    name: string;
    operator: string;
    url: string;
    phone_number: string;
    email: string;
    feed_contact_email: string;
    timezone: string;
    license_id: string;
    rental_apps: INextbikeRentalApps;
}

export interface INextbikeSystemInformationInput {
    last_updated: number;
    ttl: number;
    version: string;
    data: INextbikeSystemInformation;
}

const RentalAppsJsonSchema: JSONSchemaType<INextbikeRentalAppUris> = {
    type: "object",
    properties: {
        store_uri: {
            type: "string",
        },
        discovery_uri: {
            type: "string",
        },
    },
    required: ["store_uri", "discovery_uri"],
    additionalProperties: false,
};

const SystemInformationJsonSchema: JSONSchemaType<INextbikeSystemInformation> = {
    type: "object",
    properties: {
        system_id: {
            type: "string",
        },
        language: {
            type: "string",
        },
        name: {
            type: "string",
        },
        operator: {
            type: "string",
        },
        url: {
            type: "string",
        },
        phone_number: {
            type: "string",
        },
        email: {
            type: "string",
        },
        feed_contact_email: {
            type: "string",
        },
        timezone: {
            type: "string",
        },
        license_id: {
            type: "string",
        },
        rental_apps: {
            type: "object",
            properties: {
                android: RentalAppsJsonSchema,
                ios: RentalAppsJsonSchema,
            },
            required: ["android", "ios"],
            additionalProperties: false,
        },
    },
    required: [
        "system_id",
        "language",
        "name",
        "operator",
        "url",
        "phone_number",
        "email",
        "timezone",
        "license_id",
        "rental_apps",
    ],
    additionalProperties: true,
};

export const nextbikeSystemInfoJsonSchema: ISchemaDefinition<INextbikeSystemInformationInput> = {
    name: "SharedBikesNextbikeSystemInfoDataSource",
    jsonSchema: {
        type: "object",
        properties: {
            last_updated: {
                type: "number",
            },
            ttl: {
                type: "number",
            },
            version: {
                type: "string",
            },
            data: SystemInformationJsonSchema,
        },
        required: ["last_updated", "ttl", "version", "data"],
        additionalProperties: false,
    },
};
