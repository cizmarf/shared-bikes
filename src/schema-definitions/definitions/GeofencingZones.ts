import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { MultiPolygon } from "geojson";

interface ISharedBikesGeofencingZoneOutput {
    id: string;
    system_id: string;
    name: string | null;
    note: string | null;
    source: string | null;
    price: number | null;
    priority: number;
    start: Date | null;
    end: Date | null;
    geom: MultiPolygon;
    ride_allowed: boolean;
    ride_through_allowed: boolean;
    maximum_speed_kph: number | null;
    parking_allowed: boolean;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedBikesGeofencingZoneOutput> = {
    id: {
        type: DataTypes.TEXT,
        primaryKey: true,
    },
    system_id: DataTypes.STRING(50),
    name: DataTypes.TEXT,
    note: DataTypes.TEXT,
    source: DataTypes.TEXT,
    price: DataTypes.INTEGER,
    priority: DataTypes.INTEGER,
    start: DataTypes.DATE,
    end: DataTypes.DATE,
    geom: DataTypes.GEOMETRY,
    ride_allowed: DataTypes.BOOLEAN,
    ride_through_allowed: DataTypes.BOOLEAN,
    parking_allowed: DataTypes.BOOLEAN,
    maximum_speed_kph: DataTypes.NUMBER,

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            system_id: {
                type: "string",
            },
            name: {
                type: ["null", "string"],
            },
            note: {
                type: ["null", "string"],
            },
            source: {
                type: ["null", "string"],
            },
            price: {
                type: ["null", "number"],
            },
            priority: {
                type: ["number"],
            },
            start: {
                type: ["null", "object"],
            },
            end: {
                type: ["null", "object"],
            },
            geom: {
                type: "object",
            },
            ride_allowed: {
                type: "boolean",
            },
            ride_through_allowed: {
                type: "boolean",
            },
            parking_allowed: {
                type: "boolean",
            },
            maximum_speed_kph: {
                type: ["null", "number"],
            },
        },
    },
};

export const geofencingZones = {
    name: "SharedBikesGeofencingZones",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "geofencing_zones",
};

export type { ISharedBikesGeofencingZoneOutput };
