import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface ISharedBikesSystemInformationOutput {
    operator_id: string;
    system_id: string;
    language: string;
    logo: string;
    name: string;
    short_name: string | null;
    operator: string | null;
    url: string | null;
    purchase_url: string | null;
    start_date: Date | null;
    phone_number: string | null;
    email: string | null;
    feed_contact_email: string | null;
    timezone: string;
    license_id: string | null;
    license_url: string | null;
    attribution_organization_name: string | null;
    attribution_url: string | null;
    terms_of_use_url: string | null;
    rental_app_id: string | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedBikesSystemInformationOutput> = {
    operator_id: {
        type: DataTypes.STRING(50),
        primaryKey: true,
    },
    system_id: DataTypes.STRING(50),
    language: DataTypes.STRING(5),
    logo: DataTypes.TEXT,
    name: DataTypes.TEXT,
    short_name: DataTypes.TEXT,
    operator: DataTypes.TEXT,
    url: DataTypes.TEXT,
    purchase_url: DataTypes.TEXT,
    start_date: DataTypes.DATE,
    phone_number: DataTypes.TEXT,
    email: DataTypes.TEXT,
    feed_contact_email: DataTypes.TEXT,
    timezone: DataTypes.STRING(50),
    license_id: DataTypes.TEXT,
    license_url: DataTypes.TEXT,
    attribution_organization_name: DataTypes.TEXT,
    attribution_url: DataTypes.TEXT,
    terms_of_use_url: DataTypes.TEXT,
    rental_app_id: DataTypes.STRING(50),

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            operator_id: {
                type: "string",
            },
            system_id: {
                type: "string",
            },
            language: {
                type: "string",
            },
            logo: {
                type: "string",
            },
            name: {
                type: "string",
            },
            short_name: {
                type: ["null", "string"],
            },
            operator: {
                type: ["null", "string"],
            },
            url: {
                type: ["null", "string"],
            },
            purchase_url: {
                type: ["null", "string"],
            },
            start_date: {
                type: ["null", "object"],
            },
            phone_number: {
                type: ["null", "string"],
            },
            email: {
                type: ["null", "string"],
            },
            feed_contact_email: {
                type: ["null", "string"],
            },
            timezone: {
                type: "string",
            },
            license_id: {
                type: ["null", "string"],
            },
            license_url: {
                type: ["null", "string"],
            },
            attribution_organization_name: {
                type: ["null", "string"],
            },
            attribution_url: {
                type: ["null", "string"],
            },
            terms_of_use_url: {
                type: ["null", "string"],
            },
            rental_app_id: {
                type: ["null", "string"],
            },
        },
    },
};

export const systemInformation = {
    name: "SharedBikesSystemInformation",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "system_information",
};

export type { ISharedBikesSystemInformationOutput };
