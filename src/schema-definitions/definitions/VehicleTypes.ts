import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface ISharedBikesVehicleTypeOutput {
    id: string;
    form_factor: string | null;
    propulsion_type: string | null;
    max_range_meters: number | null;
    name: string | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedBikesVehicleTypeOutput> = {
    id: {
        type: DataTypes.TEXT,
        primaryKey: true,
    },
    form_factor: DataTypes.TEXT,
    propulsion_type: DataTypes.TEXT,
    max_range_meters: DataTypes.FLOAT,
    name: DataTypes.TEXT,

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: {
                type: "string",
            },
            form_factor: {
                type: "string",
            },
            propulsion_type: {
                type: "string",
            },
            max_range_meters: {
                type: ["null", "number"],
            },
            name: {
                type: ["null", "string"],
            },
        },
    },
};

export const vehicleTypes = {
    name: "SharedBikesVehicleTypes",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "vehicle_types",
};

export type { ISharedBikesVehicleTypeOutput };
