import { DataTypes, ModelAttributes } from "@golemio/core/dist/shared/sequelize";

interface ISharedBikesStationStatusVehicleTypeOutput {
    station_id: string;
    vehicle_type_id: string;
    count: number | null;
    num_bikes_available: number;
    num_bikes_disabled: number | null;
    vehicle_docks_available: number | null;
    [audit: string]: unknown;
}

const outputSequelizeAttributes: ModelAttributes<any, ISharedBikesStationStatusVehicleTypeOutput> = {
    station_id: {
        type: DataTypes.TEXT,
        primaryKey: true,
    },
    vehicle_type_id: DataTypes.TEXT,
    count: DataTypes.INTEGER,
    num_bikes_available: DataTypes.INTEGER,
    num_bikes_disabled: DataTypes.INTEGER,
    vehicle_docks_available: DataTypes.INTEGER,

    // Audit fields
    create_batch_id: DataTypes.BIGINT,
    created_at: DataTypes.DATE,
    created_by: DataTypes.STRING,
    update_batch_id: DataTypes.BIGINT,
    updated_at: DataTypes.DATE,
    updated_by: DataTypes.STRING,
};

const outputJsonSchema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            station_id: {
                type: "string",
            },
            vehicle_type_id: {
                type: "string",
            },
            count: {
                type: ["null", "number"],
            },
            num_bikes_available: {
                type: "number",
            },
            num_bikes_disabled: {
                type: ["null", "number"],
            },
            vehicle_docks_available: {
                type: ["null", "number"],
            },
        },
    },
};

export const stationsStatusVehicleType = {
    name: "SharedBikesStationStatusVehicleType",
    outputSequelizeAttributes,
    outputJsonSchema,
    pgTableName: "station_status_vehicle_type",
};

export type { ISharedBikesStationStatusVehicleTypeOutput };
