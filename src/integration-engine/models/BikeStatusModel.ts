import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class BikeStatusModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.bikeStatus.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.bikeStatus.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.bikeStatus.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.bikeStatus.name + "ModelValidator",
                SharedBikes.definitions.bikeStatus.outputJsonSchema
            )
        );
    }

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        try {
            return await this.sequelizeModel.destroy({
                where: {
                    last_reported: {
                        [Sequelize.Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                    },
                },
            });
        } catch (err) {
            throw new CustomError("Error while purging old data", true, this.constructor.name, 5002, err);
        }
    };
}
