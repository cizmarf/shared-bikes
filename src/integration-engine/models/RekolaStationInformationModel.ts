import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import Sequelize, { Model, Transaction } from "@golemio/core/dist/shared/sequelize";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { ISharedBikesBikeStatusOutput, SharedBikes } from "#sch";
import { IStationInformationElementTransformationResult } from "../transformations";
import { StationStatusModel } from "./StationStatusModel";
import { BikeStatusModel } from "./BikeStatusModel";

export class RekolaStationInformationModel extends PostgresModel implements IModel {
    private stationStatusModel: StationStatusModel;
    private bikeStatusModel: BikeStatusModel;

    constructor() {
        super(
            SharedBikes.definitions.stationInformation.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.stationInformation.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.stationInformation.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.stationInformation.name + "ModelValidator",
                SharedBikes.definitions.stationInformation.outputJsonSchema
            )
        );

        this.stationStatusModel = new StationStatusModel();
        this.bikeStatusModel = new BikeStatusModel();

        // Create associations
        this.stationStatusModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            foreignKey: "station_id",
        });
        this.sequelizeModel.hasOne(this.stationStatusModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
        });

        this.bikeStatusModel["sequelizeModel"].belongsTo(this.sequelizeModel, {
            foreignKey: "station_id",
        });
        this.sequelizeModel.hasMany(this.bikeStatusModel["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "station_id",
            as: "bike_status",
        });
    }

    public createAndAssociate = async (data: IStationInformationElementTransformationResult[]): Promise<Model[]> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            const rows = await Promise.all(
                data.map(async (item) => {
                    const prevStation: Model & {
                        getStation_status: () => Promise<Model>;
                        getBike_status: () => Promise<Model[]>;
                    } = await this.sequelizeModel.findOne({
                        include: [
                            {
                                as: "station_status",
                                model: this.stationStatusModel["sequelizeModel"],
                            },
                            {
                                as: "bike_status",
                                model: this.bikeStatusModel["sequelizeModel"],
                            },
                        ],
                        where: { id: item.id },
                        transaction: t,
                    });

                    // Stations exist in the db
                    if (prevStation) {
                        // Update associated station status
                        const prevStationStatus = await prevStation.getStation_status();
                        await prevStationStatus?.update(item.station_status, { transaction: t });

                        // Destroy old vehicle associations
                        const prevVehicles = await prevStation.getBike_status();
                        await Promise.all(prevVehicles.map((vehicle) => vehicle.destroy({ transaction: t })));

                        // Create new vehicle associations
                        await this.upsertAssociatedVehicles(item.bike_status, prevStation.getDataValue("id"), t);

                        return prevStation.update(item, { transaction: t });
                    } else {
                        const station = await this.sequelizeModel.create<Model>(item as any, {
                            include: [
                                {
                                    as: "station_status",
                                    model: this.stationStatusModel["sequelizeModel"],
                                },
                            ],
                            transaction: t,
                        });

                        await this.upsertAssociatedVehicles(item.bike_status, station.getDataValue("id"), t);
                        return station;
                    }
                })
            );

            await t.commit();
            return rows;
        } catch (err) {
            await t.rollback();
            throw new CustomError("Error while saving data", true, this.constructor.name, undefined, err);
        }
    };

    public deleteNHoursOldData = async (hours: number): Promise<number> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            const models = await this.sequelizeModel.findAll<Model>({
                attributes: { include: ["id"] },
                include: [
                    {
                        as: "station_status",
                        model: this.stationStatusModel["sequelizeModel"],
                        where: {
                            last_reported: {
                                [Sequelize.Op.lt]: Sequelize.literal(`NOW() - INTERVAL '${hours} HOURS'`),
                            },
                        },
                        attributes: [],
                    },
                ],
                transaction: t,
            });

            for (const model of models) {
                await model.destroy({ transaction: t });
            }

            await t.commit();
            return models.length;
        } catch (err) {
            await t.rollback();
            throw new CustomError("Error while purging old data", true, this.constructor.name, 5002, err);
        }
    };

    /**
     * Assign station id and upsert associated vehicles
     */
    private upsertAssociatedVehicles = (
        vehicles: ISharedBikesBikeStatusOutput[],
        stationId: string,
        transaction: Transaction
    ): Promise<Array<[Model, boolean | null]>> => {
        return Promise.all(
            vehicles.map((vehicle) =>
                this.bikeStatusModel["sequelizeModel"].upsert(
                    { ...vehicle, station_id: stationId },
                    { returning: config.NODE_ENV === "test", transaction }
                )
            )
        );
    };
}
