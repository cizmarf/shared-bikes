import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class StationInformationModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.stationInformation.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.stationInformation.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.stationInformation.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.stationInformation.name + "ModelValidator",
                SharedBikes.definitions.stationInformation.outputJsonSchema
            )
        );
    }
}
