import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { ISharedBikesSystemInformationOutput, SharedBikes } from "#sch";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { Model } from "@golemio/core/dist/shared/sequelize";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";

export class SystemInformationModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.systemInformation.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.systemInformation.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.systemInformation.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.systemInformation.name + "ModelValidator",
                SharedBikes.definitions.systemInformation.outputJsonSchema
            )
        );
    }

    public deleteBySystemId = async (systemId: string): Promise<void> => {
        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();

        try {
            const models = await this.sequelizeModel.findAll<Model>({
                where: {
                    system_id: systemId,
                },
                transaction: t,
            });

            for (const model of models) {
                await model.destroy({ transaction: t });
            }

            await t.commit();
        } catch (err) {
            await t.rollback();
            throw new CustomError("Error while deleting system_information", true, this.constructor.name, 5002, err);
        }
    };

    GetOne(id: string): Promise<ISharedBikesSystemInformationOutput> {
        return this.sequelizeModel.findOne({
            where: {
                system_id: id,
            },
            raw: true,
        });
    }
}
