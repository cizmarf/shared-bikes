import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class PricingModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.pricings.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.pricings.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.pricings.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.pricings.name + "ModelValidator",
                SharedBikes.definitions.pricings.outputJsonSchema
            )
        );
    }
}
