import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { SharedBikes } from "#sch";

export class PricingPlansModel extends PostgresModel implements IModel {
    constructor() {
        super(
            SharedBikes.definitions.pricingPlans.name + "Model",
            {
                outputSequelizeAttributes: SharedBikes.definitions.pricingPlans.outputSequelizeAttributes,
                pgTableName: SharedBikes.definitions.pricingPlans.pgTableName,
                pgSchema: SharedBikes.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                SharedBikes.definitions.pricingPlans.name + "ModelValidator",
                SharedBikes.definitions.pricingPlans.outputJsonSchema
            )
        );
    }
}
