import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { RekolaGeofencingTransformation, RekolaTrackablesTransformation } from "./transformations";
import {
    GeofencingZonesModel,
    RekolaStationInformationModel,
    StationStatusVehicleTypeModel,
    VehicleTypesModel,
    BikeStatusModel,
} from "./models";
import { RekolaGeofencingZonesDataSourceFactory } from "#ie/dataSources/RekolaGeofencingZonesDataSourceFactory";
import { RekolaTrackablesDataSourceFactory } from "#ie/dataSources/RekolaTrackablesDataSourceFactory";

export class RekolaSharedBikesWorker extends BaseWorker {
    private static COMPANY_PREFIX = "rekola-";
    private static SYSTEM_ID = "d727bb19-9755-40b2-9615-01fbb6180b8b"; // COMPANY_NAME = "Rekola"
    private static BIKE_STATUS_RENTAL_APP_ID = null;
    private static STATION_INFORMATION_RENTAL_APP_ID = null;

    private zonesDatasource: DataSource;
    private trackablesDatasource: DataSource;

    private geofencingTransformation: RekolaGeofencingTransformation;
    private trackablesTransformation: RekolaTrackablesTransformation;

    private geofencingZonesModel: GeofencingZonesModel;
    private stationInformationModel: RekolaStationInformationModel;
    private stationStatusVehicleTypesModel: StationStatusVehicleTypeModel;
    private vehicleTypesModel: VehicleTypesModel;
    private bikeStatusModel: BikeStatusModel;

    constructor() {
        super();
        this.zonesDatasource = RekolaGeofencingZonesDataSourceFactory.getDataSource();
        this.trackablesDatasource = RekolaTrackablesDataSourceFactory.getDataSource();

        this.geofencingTransformation = new RekolaGeofencingTransformation(
            RekolaSharedBikesWorker.COMPANY_PREFIX,
            RekolaSharedBikesWorker.SYSTEM_ID
        );
        this.trackablesTransformation = new RekolaTrackablesTransformation(RekolaSharedBikesWorker.COMPANY_PREFIX);

        this.geofencingZonesModel = new GeofencingZonesModel();
        this.stationInformationModel = new RekolaStationInformationModel();
        this.stationStatusVehicleTypesModel = new StationStatusVehicleTypeModel();
        this.vehicleTypesModel = new VehicleTypesModel();
        this.bikeStatusModel = new BikeStatusModel();
    }

    /**
     * Worker method - fetch new Rekola geofencing data
     */
    public refreshRekolaGeofencingData = async (): Promise<void> => {
        try {
            const { zones } = await this.geofencingTransformation.transform({
                items: await this.zonesDatasource.getAll(),
            });
            await this.geofencingZonesModel.replace(zones);
        } catch (err) {
            log.error(err);

            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while refreshing geofencing data", true, this.constructor.name, undefined, err);
            }
        }
    };

    /**
     * Worker method - fetch new Rekola trackable data
     */
    public refreshRekolaTrackableData = async (): Promise<void> => {
        try {
            const { stations, stationStatusVehicleTypes, vehicles, vehicleTypes } = await this.trackablesTransformation.transform(
                {
                    ...(await this.trackablesDatasource.getAll()),
                    systemId: RekolaSharedBikesWorker.SYSTEM_ID,
                    bikeStatusRentalAppId: RekolaSharedBikesWorker.BIKE_STATUS_RENTAL_APP_ID,
                    stationInformationRentalAppId: RekolaSharedBikesWorker.STATION_INFORMATION_RENTAL_APP_ID,
                }
            );

            await this.vehicleTypesModel.save(vehicleTypes);
            await this.stationInformationModel.createAndAssociate(stations);
            await this.stationStatusVehicleTypesModel.save(stationStatusVehicleTypes);
            await this.bikeStatusModel.save(vehicles);
        } catch (err) {
            log.error(err);

            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error while refreshing trackable data", true, this.constructor.name, undefined, err);
            }
        }
    };
}
