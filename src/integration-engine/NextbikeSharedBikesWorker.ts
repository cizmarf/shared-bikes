import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { config } from "@golemio/core/dist/integration-engine";
import {
    ISharedBikesBikeStatusOutput,
    ISharedBikesPricingOutput,
    ISharedBikesPricingPlanOutput,
    ISharedBikesRentalAppOutput,
    ISharedBikesStationInformationOutput,
    ISharedBikesStationStatusOutput,
    ISharedBikesStationStatusVehicleTypeOutput,
    ISharedBikesSystemInformationOutput,
    ISharedBikesVehicleTypeOutput,
} from "#sch";
import {
    VehicleTypesModel,
    BikeStatusModel,
    StationStatusModel,
    StationInformationModel,
    SystemInformationModel,
    PricingPlansModel,
    PricingModel,
    RentalAppsModel,
    StationStatusVehicleTypeModel,
} from "./models";
import { NEXT_BIKE_SOURCES, NextbikeDataSourceFactory } from "#ie/dataSources/NextbikeDataSourceFactory";
import { NextbikeTransformationFactory } from "#ie/transformations/NextbikeTransformationFactory";
import { INextbikeDefinitionTransformation, INextBikeStationStatusOutput } from "#ie/transformations/Nextbike";
import {
    INextBikeFreeBikeStatusOutput,
    INextBikePricingPlansOutput,
    INextBikeStationInformationOutput,
    INextBikeSystemInformationOutput,
} from "#ie/transformations";
import { getVehicleType } from "#ie/transformations/Nextbike";

export interface INextBikeTransformationResult {
    systemInformation: ISharedBikesSystemInformationOutput[];
    bikeStatus: ISharedBikesBikeStatusOutput[];
    stationInformation: ISharedBikesStationInformationOutput[];
    stationStatus: ISharedBikesStationStatusOutput[];
    stationStatusVehicleType: ISharedBikesStationStatusVehicleTypeOutput[];
    vehicleTypes: ISharedBikesVehicleTypeOutput[];
    pricingPlans: ISharedBikesPricingPlanOutput[];
    pricings: ISharedBikesPricingOutput[];
    rentalApps: ISharedBikesRentalAppOutput[];
}

export class NextbikeSharedBikesWorker extends BaseWorker {
    private config = config.datasources.NextbikeSharedBikes;
    private dataSourceFactory: NextbikeDataSourceFactory;
    private transformationFactory: NextbikeTransformationFactory;

    private systemInformationModel: SystemInformationModel;
    private pricingPlansModel: PricingPlansModel;
    private pricingModel: PricingModel;
    private rentalAppsModel: RentalAppsModel;
    private vehicleTypesModel: VehicleTypesModel;
    private bikeStatusModel: BikeStatusModel;
    private stationStatusModel: StationStatusModel;
    private stationStatusVehicleTypeModel: StationStatusVehicleTypeModel;
    private stationInformationModel: StationInformationModel;

    constructor() {
        super();
        this.dataSourceFactory = new NextbikeDataSourceFactory();
        this.transformationFactory = new NextbikeTransformationFactory();

        this.systemInformationModel = new SystemInformationModel();
        this.pricingPlansModel = new PricingPlansModel();
        this.pricingModel = new PricingModel();
        this.rentalAppsModel = new RentalAppsModel();
        this.vehicleTypesModel = new VehicleTypesModel();
        this.bikeStatusModel = new BikeStatusModel();
        this.stationStatusModel = new StationStatusModel();
        this.stationStatusVehicleTypeModel = new StationStatusVehicleTypeModel();
        this.stationInformationModel = new StationInformationModel();
    }

    /**
     * Main worker method - fetch new Nextbike data
     */
    public refreshNextbikeData = async (): Promise<void> => {
        for (const sourceId of this.config.sourceIds) {
            const definitionDatasource = this.dataSourceFactory.getDataSourceDefinition(sourceId);
            const definitionTransformation = this.transformationFactory.getDefinitionTransformation(sourceId);

            try {
                const transformedDefinitionData = await definitionTransformation.transform(await definitionDatasource.getAll());
                await this.processDataSource(sourceId, transformedDefinitionData);
            } catch (err) {
                if (err instanceof CustomError) {
                    throw err;
                } else {
                    throw new CustomError(
                        `Error while refreshing Nextbike data for ${sourceId}`,
                        true,
                        this.constructor.name,
                        undefined,
                        err
                    );
                }
            }
        }
    };

    private async processDataSource(sourceId: string, definitions: INextbikeDefinitionTransformation[]): Promise<void> {
        const result: INextBikeTransformationResult = {
            systemInformation: [],
            bikeStatus: [],
            stationInformation: [],
            stationStatus: [],
            stationStatusVehicleType: [],
            vehicleTypes: [getVehicleType()],
            pricingPlans: [],
            pricings: [],
            rentalApps: [],
        };

        for (const definition of definitions) {
            const datasource = this.dataSourceFactory.getDataSourceData(definition);
            const transformation = this.transformationFactory.getDataTransformation(sourceId, definition.name);
            const data = await transformation.transform(await datasource.getAll());

            switch (definition.name) {
                case NEXT_BIKE_SOURCES.free_bike_status:
                    result.bikeStatus.push(...(data as INextBikeFreeBikeStatusOutput).bikeStatus);
                    result.rentalApps.push(...(data as INextBikeFreeBikeStatusOutput).rentalApps);
                    break;
                case NEXT_BIKE_SOURCES.station_information:
                    result.stationInformation.push(...(data as INextBikeStationInformationOutput).stationInfo);
                    result.rentalApps.push(...(data as INextBikeStationInformationOutput).rentalApps);
                    break;
                case NEXT_BIKE_SOURCES.station_status:
                    result.stationStatus.push(...(data as INextBikeStationStatusOutput).stationStatus);
                    result.stationStatusVehicleType.push(...(data as INextBikeStationStatusOutput).stationStatusVehicleType);
                    break;
                case NEXT_BIKE_SOURCES.system_information:
                    result.systemInformation.push((data as INextBikeSystemInformationOutput).systemInfo);
                    result.rentalApps.push((data as INextBikeSystemInformationOutput).rentalApps);
                    break;
                case NEXT_BIKE_SOURCES.system_pricing_plans:
                    result.pricingPlans.push(...(data as INextBikePricingPlansOutput).pricingPlan);
                    result.pricings.push(...(data as INextBikePricingPlansOutput).pricing);
                    break;
                default:
                    throw new Error(`Invalid definition (${definition.name})`);
            }
        }

        await this.rentalAppsModel.save(result.rentalApps);
        await this.systemInformationModel.save(result.systemInformation);
        await this.stationInformationModel.save(result.stationInformation);
        await this.stationStatusModel.save(result.stationStatus);
        await this.vehicleTypesModel.save(result.vehicleTypes);
        await this.stationStatusVehicleTypeModel.save(result.stationStatusVehicleType);
        await this.bikeStatusModel.save(result.bikeStatus);
        await this.pricingPlansModel.save(result.pricingPlans);
        await this.pricingModel.save(result.pricings);
    }
}
