import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { DataSource, log } from "@golemio/core/dist/integration-engine";
import { ISharedBikesBikeStatusOutput, ISharedBikesPricingOutput, ISharedBikesPricingPlanOutput, SharedBikes } from "#sch";
import { BeRiderVehiclesTransformation } from "#ie/transformations";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { BikeStatusModel, PricingModel, PricingPlansModel, SystemInformationModel } from "#ie/models";
import { IBeRiderVehicle } from "#sch/datasources/BeRiderVehicle";
import { transformPricingPlan } from "#ie/transformations/BeRider/transformPricingPlan";
import { transformPricing } from "#ie/transformations/BeRider/transformPricing";
import { BeRiderStaticDataService } from "#ie/BeRiderStaticDataService";
import { BeRiderVehiclesDataSourceFactory } from "#ie/dataSources/BeRiderVehiclesDataSourceFactory";

export class BeRiderVehiclesWorker extends BaseWorker {
    public static readonly SYSTEM_ID = "b3fa5ba8-eabd-4dfa-ab6c-37ea764cb7c1";
    private dataSource: DataSource;
    private beRiderStaticDataService: BeRiderStaticDataService;

    constructor() {
        super();
        this.dataSource = BeRiderVehiclesDataSourceFactory.getDataSource();
        this.beRiderStaticDataService = new BeRiderStaticDataService(new SystemInformationModel());
    }

    public refreshBeRiderVehicleData = async (): Promise<void> => {
        const transformationDate = new Date();

        const vehiclesTransformation = new BeRiderVehiclesTransformation(
            SharedBikes.datasources.BeRiderVehiclesJsonSchema.name,
            BeRiderVehiclesWorker.SYSTEM_ID,
            transformationDate
        );

        try {
            await this.beRiderStaticDataService.saveBeRiderSystemBasics(BeRiderVehiclesWorker.SYSTEM_ID);

            const transformedData = await vehiclesTransformation.transform({
                vehicles: await this.getVehiclesFromDataSource(),
            });

            await this.saveBikeStatus(transformedData);
            await this.savePricingPlans(transformPricingPlan(BeRiderVehiclesWorker.SYSTEM_ID));
            await this.savePricings(transformPricing());
        } catch (err) {
            log.error(err);

            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError(
                    "Error while refreshing BeRider vehicles data",
                    true,
                    this.constructor.name,
                    undefined,
                    err
                );
            }
        }
    };

    private async saveBikeStatus(vehicles: ISharedBikesBikeStatusOutput[]): Promise<void> {
        const bikeStatusModel = new BikeStatusModel();
        await bikeStatusModel.save(vehicles);
    }

    private async savePricingPlans(pricingPlans: ISharedBikesPricingPlanOutput[]): Promise<void> {
        const pricingPlansModel = new PricingPlansModel();
        await pricingPlansModel.save(pricingPlans);
    }

    private async savePricings(pricings: ISharedBikesPricingOutput[]): Promise<void> {
        const pricingModel = new PricingModel();
        await pricingModel.save(pricings);
    }

    private async getVehiclesFromDataSource(): Promise<IBeRiderVehicle[]> {
        const allData = await this.dataSource.getAll();
        if (!Array.isArray(allData)) {
            throw new TypeError("Reveiced BeRider data is not array, it is " + typeof allData);
        }
        return allData;
    }
}
