import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import {
    IRekolaTrackablesDatasource,
    IRekolaTrackablesDatasourceRack,
    IRekolaTrackablesDatasourceVehicle,
    ISharedBikesStationInformationOutput,
    ISharedBikesStationStatusOutput,
    ISharedBikesStationStatusVehicleTypeOutput,
    ISharedBikesBikeStatusOutput,
    ISharedBikesVehicleTypeOutput,
    SharedBikes,
} from "#sch";
import { transformVehicleType } from "./Rekola/transformVehicleType";
import { transformBikeStatus } from "#ie/transformations/Rekola/transformBikeStatus";

export interface IStationTransformationResult {
    station: IStationInformationElementTransformationResult;
    vehicleTypes: ISharedBikesVehicleTypeOutput[];
    stationStatusVehicleTypes: Array<Partial<ISharedBikesStationStatusVehicleTypeOutput>>;
}

export interface IStationInformationElementTransformationResult extends Partial<ISharedBikesStationInformationOutput> {
    station_status: Partial<ISharedBikesStationStatusOutput>;
    bike_status: ISharedBikesBikeStatusOutput[];
}

interface ITransformationResult {
    stations: IStationInformationElementTransformationResult[];
    stationStatusVehicleTypes: Array<Partial<ISharedBikesStationStatusVehicleTypeOutput>>;
    vehicles: ISharedBikesBikeStatusOutput[];
    vehicleTypes: ISharedBikesVehicleTypeOutput[];
}

export class RekolaTrackablesTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly companyPrefix: string;

    constructor(companyPrefix: string) {
        super();
        this.name = SharedBikes.datasources.rekolaTrackables.name;
        this.companyPrefix = companyPrefix;
    }

    public transform = (
        data: {
            systemId: string;
            bikeStatusRentalAppId: string;
            stationInformationRentalAppId: string;
        } & IRekolaTrackablesDatasource
    ): Promise<ITransformationResult> => {
        let res: ITransformationResult = {
            stations: [],
            stationStatusVehicleTypes: [],
            vehicles: [],
            vehicleTypes: [],
        };

        // Station iterator
        for (const rack of data.racks) {
            if (!rack.isVisible) continue;
            const { station, vehicleTypes, stationStatusVehicleTypes } = this.transformStation(
                data.systemId,
                data.stationInformationRentalAppId,
                rack
            );

            res.stations.push(station);
            res.vehicleTypes = [...res.vehicleTypes, ...vehicleTypes];
            res.stationStatusVehicleTypes = [...res.stationStatusVehicleTypes, ...stationStatusVehicleTypes];
        }

        // Vehicle iterator
        for (const vehicle of data.vehicles) {
            if (!vehicle.isVisible) continue;
            res.vehicles.push(this.transformVehicle(data.systemId, data.bikeStatusRentalAppId, vehicle));
            res.vehicleTypes.push(this.transformVehicleType(vehicle));
        }

        // Filter out redundant vehicle types
        res.vehicleTypes = [...new Map(res.vehicleTypes.map((type) => [type.id, type])).values()];
        return Promise.resolve(res);
    };

    // Do nothing
    protected transformElement = Function;

    /**
     * Transform stations (racks)
     */
    private transformStation = (
        systemId: string,
        rentalAppId: string,
        element: IRekolaTrackablesDatasourceRack
    ): IStationTransformationResult => {
        let vehicles: ISharedBikesBikeStatusOutput[] = [];
        let vehicleTypes: ISharedBikesVehicleTypeOutput[] = [];

        // Vehicle iterator
        for (const vehicle of element.vehicles) {
            if (!vehicle.isVisible) continue;
            vehicles.push(this.transformVehicle(systemId, rentalAppId, vehicle));
            vehicleTypes.push(this.transformVehicleType(vehicle));
        }

        const stationStatusVehicleTypes: Array<Partial<ISharedBikesStationStatusVehicleTypeOutput>> = vehicleTypes.map(
            (vehicleType) => ({
                station_id: this.companyPrefix + element.id,
                vehicle_type_id: vehicleType.id,
                count: element.vehicles.length,
                num_bikes_available: vehicles.filter((vehicle) => vehicle.vehicle_type_id === vehicleType.id).length,
            })
        );

        return {
            station: {
                id: this.companyPrefix + element.id,
                system_id: systemId,
                name: element.name,
                point: {
                    type: "Point",
                    coordinates: [element.position.lng, element.position.lat],
                },
                is_virtual_station: true,
                rental_app_id: rentalAppId,
                station_status: {
                    num_bikes_available: element.vehicles.length,
                    is_installed: true,
                    is_renting: element.vehicles.length > 0,
                    is_returning: true,
                    last_reported: new Date().toISOString(),
                },
                bike_status: vehicles,
            },
            vehicleTypes,
            stationStatusVehicleTypes,
        };
    };

    /**
     * Transform vehicle
     */
    private transformVehicle = (
        systemId: string,
        rentalAppId: string,
        element: IRekolaTrackablesDatasourceVehicle
    ): ISharedBikesBikeStatusOutput => {
        const transformationDate = new Date();
        return transformBikeStatus(systemId, rentalAppId, element, transformationDate);
    };

    /**
     * Transport vehicle type (bike etc)
     */
    private transformVehicleType = (element: IRekolaTrackablesDatasourceVehicle): ISharedBikesVehicleTypeOutput => {
        return transformVehicleType(element);
    };
}
