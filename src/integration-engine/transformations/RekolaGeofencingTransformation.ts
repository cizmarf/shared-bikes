import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import {
    IRekolaGeofencingDatasourceItem,
    ISharedBikesGeofencingZoneOutput,
    IRekolaGeofencingDatasourceZone,
    SharedBikes,
} from "#sch";
import { transformGeofencingZone } from "#ie/transformations/Rekola/transformGeofencingZone";

interface ITransformationResult {
    zones: ISharedBikesGeofencingZoneOutput[];
}

export class RekolaGeofencingTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly companyPrefix: string;
    private readonly systemId: string;

    constructor(companyPrefix: string, systemId: string) {
        super();
        this.name = SharedBikes.datasources.rekolaGeofencingZones.name;
        this.companyPrefix = companyPrefix;
        this.systemId = systemId;
    }

    public transform = (data: { items: IRekolaGeofencingDatasourceItem[] }): Promise<ITransformationResult> => {
        let res: ITransformationResult = {
            zones: [],
        };

        for (const item of data.items) {
            for (const zone of item.zones) {
                res.zones.push(this.transformElement(zone));
            }
        }

        return Promise.resolve(res);
    };

    /**
     * Transform zones
     */
    protected transformElement = (element: IRekolaGeofencingDatasourceZone): ISharedBikesGeofencingZoneOutput => {
        return transformGeofencingZone(this.systemId, element);
    };
}
