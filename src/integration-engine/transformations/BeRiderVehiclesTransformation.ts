import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { IBeRiderVehicle } from "#sch/datasources/BeRiderVehicle";
import { ISharedBikesBikeStatusOutput } from "#sch";
import { transformBikeStatus } from "#ie/transformations/BeRider/transformBikeStatus";

export class BeRiderVehiclesTransformation extends BaseTransformation implements ITransformation {
    public readonly name: string;
    private readonly transformationDate: Date;
    private readonly systemId: string;

    constructor(jsonSchemaName: string, systemId: string, transformationDate: Date) {
        super();
        this.name = jsonSchemaName;
        this.systemId = systemId;
        this.transformationDate = transformationDate;
    }

    public transform = async (data: { vehicles: IBeRiderVehicle[] }): Promise<ISharedBikesBikeStatusOutput[]> => {
        return data.vehicles.map(this.transformElement);
    };

    public transformElement = (vehicle: IBeRiderVehicle): ISharedBikesBikeStatusOutput => {
        return transformBikeStatus(vehicle, this.systemId, this.transformationDate);
    };
}
