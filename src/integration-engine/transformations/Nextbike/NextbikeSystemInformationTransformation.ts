import getUuidByString from "uuid-by-string";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import {
    ISharedBikesPricingOutput,
    ISharedBikesPricingPlanOutput,
    ISharedBikesRentalAppOutput,
    ISharedBikesSystemInformationOutput,
    SharedBikes,
} from "#sch";
import { INextbikeRentalApps, INextbikeSystemInformation, INextbikeSystemInformationInput } from "#sch/datasources";
import { NEXTBIKE_INTEGRATION_DATE } from "#ie/transformations/Nextbike/transformConstants";

export interface INextBikeSystemInformationOutput {
    systemInfo: ISharedBikesSystemInformationOutput;
    rentalApps: ISharedBikesRentalAppOutput;
}

export class NextbikeSystemInformationTransformation extends BaseTransformation implements ITransformation {
    public name: string;
    private readonly systemId: string;
    private readonly transformationDate: Date;

    constructor(sourceId: string) {
        super();
        this.name = SharedBikes.datasources.nextbikeSystemInfoJsonSchema.name + sourceId + "Transformation";
        this.systemId = getUuidByString(sourceId);
        this.transformationDate = new Date(NEXTBIKE_INTEGRATION_DATE);
    }

    public transform = async (datasourceData: INextbikeSystemInformationInput): Promise<INextBikeSystemInformationOutput> => {
        return this.transformElement(datasourceData.data);
    };

    protected transformElement = (item: INextbikeSystemInformation) => {
        const rentalAppId = getUuidByString(JSON.stringify(item.rental_apps));

        return {
            systemInfo: {
                operator_id: item.system_id,
                system_id: this.systemId,
                language: item.language,
                logo: "https://upload.wikimedia.org/wikipedia/commons/f/f3/Nextbike_Logo.svg",
                name: item.name,
                short_name: null,
                operator: item.operator,
                url: item.url,
                purchase_url: null,
                start_date: this.transformationDate,
                phone_number: item.phone_number,
                email: item.email,
                feed_contact_email: null,
                timezone: item.timezone,
                license_id: item.license_id,
                license_url: null,
                attribution_organization_name: null,
                attribution_url: null,
                terms_of_use_url: "https://www.nextbikeczech.com/en/general-terms-and-conditions/",
                rental_app_id: rentalAppId,
            },
            rentalApps: this.transformRentalApps(item.rental_apps, rentalAppId),
        };
    };

    private transformRentalApps = (data: INextbikeRentalApps, id: string): ISharedBikesRentalAppOutput => {
        return {
            id,
            android_store_url: data.android.store_uri,
            android_discovery_url: data.android.discovery_uri,
            ios_store_url: data.ios.store_uri,
            ios_discovery_url: data.ios.discovery_uri,
            web_url: null,
        };
    };
}
