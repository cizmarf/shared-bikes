import { IRekolaTrackablesDatasourceVehicle, ISharedBikesVehicleTypeOutput } from "#sch";

export function transformVehicleType(element: IRekolaTrackablesDatasourceVehicle): ISharedBikesVehicleTypeOutput {
    const vehicleType = element.type === "bike" ? "bicycle" : element.type;
    return {
        id: "rekola-" + element.type,
        form_factor: vehicleType,
        propulsion_type: "human",
        max_range_meters: null,
        name: vehicleType,
    };
}
