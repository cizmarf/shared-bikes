import { IRekolaTrackablesDatasourceVehicle, ISharedBikesBikeStatusOutput } from "#sch";

export function transformBikeStatus(
    systemId: string,
    rentalAppId: string,
    element: IRekolaTrackablesDatasourceVehicle,
    transformationDate: Date
): ISharedBikesBikeStatusOutput {
    return {
        id: "rekola-" + element.id,
        system_id: systemId,
        point: {
            type: "Point",
            coordinates: [element.position.lng, element.position.lat],
        },
        helmets: null,
        passengers: null,
        damage_description: null,
        description: element.label,
        vehicle_registration: null,
        is_reserved: element.isBorrowed,
        is_disabled: false,
        vehicle_type_id: "rekola-" + element.type,
        last_reported: transformationDate.toISOString(),
        current_range_meters: null,
        charge_percent: null,
        rental_app_id: rentalAppId,
        station_id: null,
    };
}
