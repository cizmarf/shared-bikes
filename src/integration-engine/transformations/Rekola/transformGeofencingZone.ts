import { IRekolaGeofencingDatasourceZone, ISharedBikesGeofencingZoneOutput } from "#sch";

export function transformGeofencingZone(
    systemId: string,
    element: IRekolaGeofencingDatasourceZone
): ISharedBikesGeofencingZoneOutput {
    return {
        id: "rekola-" + element.id,
        system_id: systemId,
        name: null,
        note: null,
        source: null,
        price: null,
        priority: 0,
        start: null,
        end: null,
        geom: {
            type: "MultiPolygon",
            coordinates: [[element.points.map(([lat, lng]) => [lng, lat])]],
        },
        ride_allowed: true,
        ride_through_allowed: true,
        maximum_speed_kph: null,
        parking_allowed: true,
    };
}
