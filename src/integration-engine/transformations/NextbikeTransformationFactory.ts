import {
    NextbikeDefinitionTransformation,
    NextbikeFreeBikeStatusTransformation,
    NextbikeStationInformationTransformation,
    NextbikeStationStatusTransformation,
    NextbikeSystemInformationTransformation,
    NextbikeSystemPricingPlansTransformation,
} from "#ie/transformations/Nextbike";
import { NEXT_BIKE_SOURCES } from "#ie/dataSources/NextbikeDataSourceFactory";

export class NextbikeTransformationFactory {
    public getDefinitionTransformation(sourceId: string): NextbikeDefinitionTransformation {
        return new NextbikeDefinitionTransformation(sourceId);
    }

    public getDataTransformation(sourceId: string, name: keyof typeof NEXT_BIKE_SOURCES) {
        switch (name) {
            case NEXT_BIKE_SOURCES.free_bike_status:
                return new NextbikeFreeBikeStatusTransformation(sourceId);
            case NEXT_BIKE_SOURCES.station_information:
                return new NextbikeStationInformationTransformation(sourceId);
            case NEXT_BIKE_SOURCES.station_status:
                return new NextbikeStationStatusTransformation(sourceId);
            case NEXT_BIKE_SOURCES.system_information:
                return new NextbikeSystemInformationTransformation(sourceId);
            case NEXT_BIKE_SOURCES.system_pricing_plans:
                return new NextbikeSystemPricingPlansTransformation(sourceId);
            default:
                throw new Error(`Invalid transformation type (${name})`);
        }
    }
}
