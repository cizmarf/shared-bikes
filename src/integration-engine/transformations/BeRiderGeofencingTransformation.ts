import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { BeRiderGeofencingZone } from "#sch/datasources/BeRiderGeofencingZone";
import { ISharedBikesGeofencingZoneOutput } from "#sch";
import { transformGeofencingZone } from "#ie/transformations/BeRider/transformGeofencingZone";

export class BeRiderGeofencingTransformation extends BaseTransformation implements ITransformation {
    public readonly name;
    private readonly systemId;

    constructor(jsonSchemaName: string, systemId: string) {
        super();
        this.name = jsonSchemaName;
        this.systemId = systemId;
    }

    public transform = async (data: { zones: BeRiderGeofencingZone[] }): Promise<ISharedBikesGeofencingZoneOutput[]> => {
        return data.zones.map(this.transformElement);
    };

    public transformElement = (zone: BeRiderGeofencingZone): ISharedBikesGeofencingZoneOutput => {
        return transformGeofencingZone(zone, this.systemId);
    };
}
