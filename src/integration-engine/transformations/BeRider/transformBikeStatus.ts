import { ISharedBikesBikeStatusOutput } from "#sch";
import { getWhereGeometry } from "#ie/transformations/helpers/getWhereGeometry";
import { IBeRiderVehicle } from "#sch/datasources/BeRiderVehicle";

export function transformBikeStatus(
    vehicle: IBeRiderVehicle,
    systemInformationId: string,
    currentDate: Date
): ISharedBikesBikeStatusOutput {
    return {
        id: "berider-" + vehicle.id,
        system_id: systemInformationId,
        point: getWhereGeometry(vehicle.lat, vehicle.lon),
        helmets: vehicle.helmets,
        passengers: 2,
        damage_description: vehicle.damageDescription,
        description: vehicle.address,
        vehicle_registration: vehicle.vehicleRegistration,
        is_reserved: isVehicleReserved(vehicle),
        is_disabled: false,
        vehicle_type_id: "berider-moped",
        last_reported: currentDate.toISOString(),
        current_range_meters: vehicle.estimatedRange,
        charge_percent: vehicle.charge,
        rental_app_id: null,
        station_id: null,
    };
}

function isVehicleReserved(vehicle: IBeRiderVehicle): boolean {
    return vehicle.reservationStateId != 0;
}
