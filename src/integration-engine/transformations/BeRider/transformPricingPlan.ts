import { ISharedBikesPricingPlanOutput } from "#sch";

export function transformPricingPlan(systemId: string): ISharedBikesPricingPlanOutput[] {
    return [
        {
            id: "b240d38c-68f7-4421-bd2c-252f0ef31dd8",
            system_id: systemId,
            url: "https://www.be-rider.com/cenik",
            last_updated: "2022-04-29T08:52:41+0000",
            name: "Ceník BeRider",
            currency: "CZK",
            price: 0,
            is_taxable: false,
            description: "Základní cena: 5 CZK/min; K dispozici také předplacené zvýhodněné balíčky na 20, 100, 200 a 300 minut",
            surge_pricing: false,
        },
    ];
}
