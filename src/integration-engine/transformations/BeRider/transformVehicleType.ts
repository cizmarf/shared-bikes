import { ISharedBikesVehicleTypeOutput } from "#sch";

export function transformVehicleType(): ISharedBikesVehicleTypeOutput {
    return {
        id: "berider-moped",
        form_factor: "shared_moped",
        propulsion_type: "electric",
        max_range_meters: 70000,
        name: "BeRider Scooter",
    };
}
