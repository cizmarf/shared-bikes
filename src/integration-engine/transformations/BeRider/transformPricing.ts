import { ISharedBikesPricingOutput } from "#sch";

export function transformPricing(): ISharedBikesPricingOutput[] {
    return [
        {
            pricing_plan_id: "b240d38c-68f7-4421-bd2c-252f0ef31dd8",
            pricing_type: "per_min_pricing",
            pricing_order: 0,
            start: 0,
            rate: 5.0,
            interval: 1,
            end: null,
            start_time_of_period: null,
            end_time_of_period: null,
        },
        {
            pricing_plan_id: "b240d38c-68f7-4421-bd2c-252f0ef31dd8",
            pricing_type: "per_min_parking_pricing",
            pricing_order: 0,
            start: 0,
            rate: 2.0,
            interval: 1,
            end: null,
            start_time_of_period: null,
            end_time_of_period: null,
        },
    ];
}
