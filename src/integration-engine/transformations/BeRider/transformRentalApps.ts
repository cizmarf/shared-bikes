import { ISharedBikesRentalAppOutput } from "#sch";

export function transformRentalApps(): ISharedBikesRentalAppOutput {
    return {
        id: "dc601405-2333-49b5-9ffe-8f76ddc09ed7",
        android_store_url: "https://play.google.com/store/apps/details?id=com.berider.app",
        android_discovery_url: "https://beridercz.page.link/launch",
        ios_store_url: "https://apps.apple.com/cz/app/berider-sd%C3%ADlen%C3%A9-sk%C3%BAtry/id1465804731",
        ios_discovery_url: "https://beridercz.page.link/launch",
        web_url: null,
    };
}
