import { BeRiderGeofencingZone, BeRiderGeoJsonFeatures } from "#sch/datasources/BeRiderGeofencingZone";
import { ISharedBikesGeofencingZoneOutput } from "#sch";
import { MultiPolygon, Position } from "geojson";

export function transformGeofencingZone(zone: BeRiderGeofencingZone, systemId: string): ISharedBikesGeofencingZoneOutput {
    return {
        id: `berider-zone-${zone.hash}`,
        system_id: systemId,
        name: `berider-zone-${zone.hash}`,
        note: null,
        source: null,
        price: null,
        priority: 0,
        start: null,
        end: null,
        geom: getMultiPolygon(zone),
        ride_allowed: true,
        ride_through_allowed: true,
        maximum_speed_kph: null,
        parking_allowed: true,
    };
}

function getMultiPolygon(zone: BeRiderGeofencingZone): MultiPolygon {
    let positions = zone.geoJson.features.map(getPositions);
    return {
        type: "MultiPolygon",
        coordinates: positions,
    };
}

function getPositions(feature: BeRiderGeoJsonFeatures): Position[][] {
    return feature.geometry.coordinates;
}
