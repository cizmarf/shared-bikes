import { WhereGeometryOptions } from "@golemio/core/dist/shared/sequelize";

export function getWhereGeometry(lat: number, lon: number): WhereGeometryOptions {
    return {
        type: "Point",
        coordinates: [lon, lat],
    };
}
