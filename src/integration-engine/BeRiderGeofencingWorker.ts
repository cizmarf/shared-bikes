import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { DataSource, log } from "@golemio/core/dist/integration-engine";
import { ISharedBikesGeofencingZoneOutput, SharedBikes } from "#sch";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { BeRiderGeofencingTransformation } from "#ie/transformations/BeRiderGeofencingTransformation";
import { BeRiderGeofencingZone } from "#sch/datasources/BeRiderGeofencingZone";
import { GeofencingZonesModel, SystemInformationModel } from "#ie/models";
import { BeRiderStaticDataService } from "#ie/BeRiderStaticDataService";
import { BeRiderGeofencingZonesDataSourceFactory } from "#ie/dataSources/BeRiderGeofencingZonesDataSourceFactory";

export class BeRiderGeofencingWorker extends BaseWorker {
    public static readonly SYSTEM_ID = "b3fa5ba8-eabd-4dfa-ab6c-37ea764cb7c1";
    private dataSource: DataSource;
    private beRiderStaticDataService: BeRiderStaticDataService;

    constructor() {
        super();
        this.dataSource = BeRiderGeofencingZonesDataSourceFactory.getDataSource();
        this.beRiderStaticDataService = new BeRiderStaticDataService(new SystemInformationModel());
    }

    public refreshBeRiderGeofencingData = async (): Promise<void> => {
        const geofencingTransformation = new BeRiderGeofencingTransformation(
            SharedBikes.datasources.BeRiderGeofencingJsonSchema.name,
            BeRiderGeofencingWorker.SYSTEM_ID
        );

        try {
            await this.beRiderStaticDataService.saveBeRiderSystemBasics(BeRiderGeofencingWorker.SYSTEM_ID);

            const transformedData = await geofencingTransformation.transform({
                zones: await this.getGeofencingZonesFromDataSource(),
            });

            await this.saveGeofencingZones(transformedData);
        } catch (err) {
            log.error(err);

            if (err instanceof CustomError) {
                throw err;
            } else {
                throw new CustomError("Error refreshing BeRider geofencing data", true, this.constructor.name, undefined, err);
            }
        }
    };

    private async getGeofencingZonesFromDataSource(): Promise<BeRiderGeofencingZone[]> {
        const allData = await this.dataSource.getAll();
        if (!Array.isArray(allData)) {
            throw new TypeError("Reveiced BeRider geofencing data is not array, it is " + typeof allData);
        }
        return allData;
    }

    private async saveGeofencingZones(transformedData: ISharedBikesGeofencingZoneOutput[]) {
        const geofencingZonesModel = new GeofencingZonesModel();
        await geofencingZonesModel.save(transformedData);
    }
}
