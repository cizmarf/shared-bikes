import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { log } from "@golemio/core/dist/integration-engine/helpers";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { RekolaStationInformationModel, BikeStatusModel } from "./models";

export class SharedBikesWorker extends BaseWorker {
    private stationInformationModel: RekolaStationInformationModel;
    private bikeStatusModel: BikeStatusModel;

    constructor() {
        super();

        this.stationInformationModel = new RekolaStationInformationModel();
        this.bikeStatusModel = new BikeStatusModel();
    }

    /**
     * Worker method - delete old trackable data
     */
    public deleteOldTrackableData = async (msg: { content: Buffer }): Promise<void> => {
        let targetHours: number;

        try {
            targetHours = +JSON.parse(msg.content.toString()).targetHours;

            // Target hours should be an integer
            // Also disallow deletion of new data
            if (!Number.isInteger(targetHours) || targetHours < 24) {
                log.warn("New data cannot be deleted. Fallbacking to 24 hours");
                targetHours = 24;
            }
        } catch (err) {
            throw new CustomError("Error while parsing cron message", true, this.constructor.name, 5050, err);
        }

        await this.stationInformationModel.deleteNHoursOldData(targetHours);
        await this.bikeStatusModel.deleteNHoursOldData(targetHours);
    };
}
