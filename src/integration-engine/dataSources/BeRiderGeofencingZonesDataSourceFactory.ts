import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class BeRiderGeofencingZonesDataSourceFactory {
    public static readonly API_HEADERS = config.datasources.BeRiderGeofencingHeaders;
    public static readonly API_URL = config.datasources.BeRiderGeofencingApiUrl;

    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.BeRiderGeofencingJsonSchema.name,
            new HTTPProtocolStrategy({
                headers: BeRiderGeofencingZonesDataSourceFactory.API_HEADERS,
                method: "GET",
                url: BeRiderGeofencingZonesDataSourceFactory.API_URL,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.BeRiderGeofencingJsonSchema.name,
                SharedBikes.datasources.BeRiderGeofencingJsonSchema.jsonSchema
            )
        );
    }
}
