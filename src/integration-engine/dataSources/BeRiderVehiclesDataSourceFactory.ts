import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class BeRiderVehiclesDataSourceFactory {
    public static readonly API_HEADERS = config.datasources.BeRiderVehiclesHeaders;
    public static readonly API_URL = config.datasources.BeRiderVehiclesApiUrl;

    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.BeRiderVehiclesJsonSchema.name,
            new HTTPProtocolStrategy({
                headers: BeRiderVehiclesDataSourceFactory.API_HEADERS,
                method: "GET",
                url: BeRiderVehiclesDataSourceFactory.API_URL,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.BeRiderVehiclesJsonSchema.name,
                SharedBikes.datasources.BeRiderVehiclesJsonSchema.jsonSchema
            )
        );
    }
}
