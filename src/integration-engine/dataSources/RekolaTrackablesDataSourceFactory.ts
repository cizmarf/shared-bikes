import { config, DataSource, HTTPProtocolStrategy, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine";
import { SharedBikes } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class RekolaTrackablesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            SharedBikes.datasources.rekolaTrackables.name,
            new HTTPProtocolStrategy({
                headers: config.datasources.RekolaSharedBikesHeaders,
                method: "GET",
                url:
                    config.datasources.RekolaSharedBikesBaseUrl +
                    "/trackables?mapLat=0&mapLng=0&mapZoom=0&gpsLat=0&gpsLng=0&gpsAcc=0",
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                SharedBikes.datasources.rekolaTrackables.name,
                SharedBikes.datasources.rekolaTrackables.jsonSchema
            )
        );
    }
}
