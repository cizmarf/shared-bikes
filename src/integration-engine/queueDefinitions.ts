import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";
import { SharedBikes } from "#sch/index";
import { SharedBikesWorker } from "#ie/SharedBikesWorker";
import { RekolaSharedBikesWorker } from "#ie/RekolaSharedBikesWorker";
import { NextbikeSharedBikesWorker } from "#ie/NextbikeSharedBikesWorker";
import { BeRiderVehiclesWorker } from "#ie/BeRiderVehiclesWorker";
import { BeRiderGeofencingWorker } from "#ie/BeRiderGeofencingWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "deleteOldTrackableData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                },
                worker: SharedBikesWorker,
                workerMethod: "deleteOldTrackableData",
            },
        ],
    },
    {
        name: "Rekola" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".rekola" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshRekolaGeofencingData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 10 * 60 * 1000, // 10 minutes
                },
                worker: RekolaSharedBikesWorker,
                workerMethod: "refreshRekolaGeofencingData",
            },
            {
                name: "refreshRekolaTrackableData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 1 * 60 * 1000, // 1 minute
                },
                worker: RekolaSharedBikesWorker,
                workerMethod: "refreshRekolaTrackableData",
            },
        ],
    },
    {
        name: "Nextbike" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".nextbike" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshNextbikeData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 1 * 60 * 1000, // 1 minute
                },
                worker: NextbikeSharedBikesWorker,
                workerMethod: "refreshNextbikeData",
            },
        ],
    },
    {
        name: "BeRider" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".berider" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshBeRiderVehicleData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 2 * 60 * 1000, // 1 minute
                },
                worker: BeRiderVehiclesWorker,
                workerMethod: "refreshBeRiderVehicleData",
            },
        ],
    },
    {
        name: "BeRider" + SharedBikes.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + ".berider" + SharedBikes.name.toLowerCase(),
        queues: [
            {
                name: "refreshBeRiderGeofencingData",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 2 * 60 * 1000, // 2 minutes
                },
                worker: BeRiderGeofencingWorker,
                workerMethod: "refreshBeRiderGeofencingData",
            },
        ],
    },
];

export { queueDefinitions };
