import { transformRentalApps } from "#ie/transformations/BeRider/transformRentalApps";
import { transformSystemInformation } from "#ie/transformations/BeRider/transformSystemInformation";
import { transformVehicleType } from "#ie/transformations/BeRider/transformVehicleType";
import { ISharedBikesRentalAppOutput, ISharedBikesSystemInformationOutput, ISharedBikesVehicleTypeOutput } from "#sch";
import { RentalAppsModel, SystemInformationModel, VehicleTypesModel } from "#ie/models";

export class BeRiderStaticDataService {
    private systemInformationModel: SystemInformationModel;

    constructor(systemInformationModel: SystemInformationModel) {
        this.systemInformationModel = systemInformationModel;
    }

    public async saveBeRiderSystemBasics(systemId: string): Promise<void> {
        const systemInformation = await this.systemInformationModel.GetOne(systemId);
        if (systemInformation === null) {
            await this.saveRentalApps([transformRentalApps()]);
            await this.saveSystemInformation(transformSystemInformation(systemId));
            await this.saveVehicleTypes([transformVehicleType()]);
        }
    }

    private async saveVehicleTypes(vehicleTypes: ISharedBikesVehicleTypeOutput[]): Promise<void> {
        const vehicleTypesModel = new VehicleTypesModel();
        await vehicleTypesModel.save(vehicleTypes);
    }

    private async saveRentalApps(rentalApps: ISharedBikesRentalAppOutput[]): Promise<void> {
        const rentalAppsModel = new RentalAppsModel();
        await rentalAppsModel.save(rentalApps);
    }

    private async saveSystemInformation(systemInformation: ISharedBikesSystemInformationOutput): Promise<void> {
        await this.systemInformationModel.save([systemInformation]);
    }
}
