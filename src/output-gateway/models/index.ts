import { BikeStatusModel } from "./BikeStatusModel";
import { GeofencingZonesModel } from "./GeofencingZonesModel";
import { PricingModel } from "./PricingModel";
import { PricingPlanModel } from "./PricingPlanModel";
import { RentalAppModel } from "./RentalAppModel";
import { StationInformationModel } from "./StationInformationModel";
import { StationStatusModel } from "./StationStatusModel";
import { StationStatusVehicleTypeModel } from "./StationStatusVehicleTypeModel";
import { SystemInformationModel } from "./SystemInformationModel";
import { VehicleTypesModel } from "./VehicleTypesModel";

export interface IGBFSModels {
    BikeStatusModel: BikeStatusModel;
    GeofencingZonesModel: GeofencingZonesModel;
    PricingModel: PricingModel;
    PricingPlanModel: PricingPlanModel;
    RentalAppModel: RentalAppModel;
    StationInformationModel: StationInformationModel;
    StationStatusModel: StationStatusModel;
    StationStatusVehicleTypeModel: StationStatusVehicleTypeModel;
    SystemInformationModel: SystemInformationModel;
    VehicleTypesModel: VehicleTypesModel;
}

const models: IGBFSModels = {
    BikeStatusModel: new BikeStatusModel(),
    GeofencingZonesModel: new GeofencingZonesModel(),
    PricingModel: new PricingModel(),
    PricingPlanModel: new PricingPlanModel(),
    RentalAppModel: new RentalAppModel(),
    StationInformationModel: new StationInformationModel(),
    StationStatusModel: new StationStatusModel(),
    StationStatusVehicleTypeModel: new StationStatusVehicleTypeModel(),
    SystemInformationModel: new SystemInformationModel(),
    VehicleTypesModel: new VehicleTypesModel(),
};

for (const type of Object.keys(models)) {
    const model = (models as any)[type];
    if (model.hasOwnProperty("Associate")) {
        model.Associate(models);
    }
}

export { models };
