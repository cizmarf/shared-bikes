import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { SharedBikes } from "#sch";

/**
 * Custom Postgres model for rental apps
 */
export class RentalAppModel extends SequelizeModel {
    constructor() {
        super(
            SharedBikes.definitions.rentalApps.name + "Model",
            SharedBikes.definitions.rentalApps.pgTableName,
            SharedBikes.definitions.rentalApps.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    async GetAll(options?: any): Promise<any[]> {
        throw new Error("Method not implemented.");
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
