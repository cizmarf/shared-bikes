import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SharedBikes } from "#sch";
import { IStationInformation, IStationInformationsJson } from "#og/definitions";
import { IGBFSModels } from "./";
import { RentalAppModel } from "./RentalAppModel";
import { removeEmptyOrNull } from "#og/helpers";

/**
 * Custom Postgres model for station information
 */
export class StationInformationModel extends SequelizeModel {
    private rentalAppModel: RentalAppModel | undefined;

    constructor() {
        super(
            SharedBikes.definitions.stationInformation.name + "Model",
            SharedBikes.definitions.stationInformation.pgTableName,
            SharedBikes.definitions.stationInformation.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.rentalAppModel = models.RentalAppModel;

        this.sequelizeModel.belongsTo(models.RentalAppModel.sequelizeModel, {
            as: "rental_uris",
            targetKey: "id",
            foreignKey: "rental_app_id",
        });
    };

    async GetAll(systemId: string): Promise<IStationInformationsJson> {
        const results = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    ["id", "station_id"],
                    [Sequelize.literal("ST_Y(point)"), "lat"],
                    [Sequelize.literal("ST_X(point)"), "lon"],
                ],
                exclude: ["id", "system_id", "point", "rental_app_id"],
            },
            include: {
                as: "rental_uris",
                model: this.rentalAppModel?.sequelizeModel,
                attributes: [
                    ["android_discovery_url", "android"],
                    ["ios_discovery_url", "ios"],
                    ["web_url", "web"],
                ],
            },
            where: {
                system_id: systemId,
            },
            nest: true,
        });

        return {
            stations: results.map((res) => removeEmptyOrNull(res.toJSON())) as IStationInformation[],
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
