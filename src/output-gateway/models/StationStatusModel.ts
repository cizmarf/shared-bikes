import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SharedBikes } from "#sch";
import { IStationStatusJson } from "#og/definitions";
import { IGBFSModels } from ".";
import { StationStatusVehicleTypeModel } from "./StationStatusVehicleTypeModel";
import { BikeStatusModel } from "./BikeStatusModel";
import { StationInformationModel } from "./StationInformationModel";
import { removeEmptyOrNull } from "#og/helpers";

/**
 * Custom Postgres model for station status
 */
export class StationStatusModel extends SequelizeModel {
    private stationStatusVehicleTypeModel: StationStatusVehicleTypeModel | undefined;
    private bikeStatusModel: BikeStatusModel | undefined;
    private stationInformationModel: StationInformationModel | undefined;

    constructor() {
        super(
            SharedBikes.definitions.stationStatus.name + "Model",
            SharedBikes.definitions.stationStatus.pgTableName,
            SharedBikes.definitions.stationStatus.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.stationStatusVehicleTypeModel = models.StationStatusVehicleTypeModel;
        this.bikeStatusModel = models.BikeStatusModel;
        this.stationInformationModel = models.StationInformationModel;

        this.sequelizeModel.hasMany(models.StationStatusVehicleTypeModel.sequelizeModel, {
            sourceKey: "station_id",
            foreignKey: "station_id",
            as: "vehicle_types_available",
        });
        this.sequelizeModel.hasMany(models.BikeStatusModel.sequelizeModel, {
            sourceKey: "station_id",
            foreignKey: "station_id",
            as: "bikes",
        });
        this.sequelizeModel.belongsTo(models.StationInformationModel.sequelizeModel, {
            targetKey: "id",
            foreignKey: "station_id",
        });
    };

    async GetAll(systemId: string): Promise<IStationStatusJson> {
        const results = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    [
                        Sequelize.literal("extract(epoch from date_trunc('second', station_status.last_reported))"),
                        "last_reported",
                    ],
                ],
            },
            include: [
                {
                    as: "vehicle_types_available",
                    model: this.stationStatusVehicleTypeModel?.sequelizeModel,
                    attributes: ["vehicle_type_id", "count"],
                },
                {
                    as: "bikes",
                    model: this.bikeStatusModel?.sequelizeModel,
                    attributes: [],
                },
                {
                    model: this.stationInformationModel?.sequelizeModel,
                    attributes: [],
                    where: {
                        system_id: systemId,
                    },
                },
            ],
        });

        return {
            stations: results.map((res) => removeEmptyOrNull(res.toJSON())),
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
