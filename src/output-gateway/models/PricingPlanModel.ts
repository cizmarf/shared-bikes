import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SharedBikes } from "#sch";
import { IPricing, ISystemPricingPlan, ISystemPricingPlanJson } from "#og/definitions";
import { IGBFSModels } from ".";
import { PricingModel } from "./PricingModel";
import { removeEmptyOrNull } from "#og/helpers";

/**
 * Custom Postgres model for pricing plan
 */
export class PricingPlanModel extends SequelizeModel {
    private pricingModel: PricingModel | undefined;

    constructor() {
        super(
            SharedBikes.definitions.pricingPlans.name + "Model",
            SharedBikes.definitions.pricingPlans.pgTableName,
            SharedBikes.definitions.pricingPlans.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.pricingModel = models.PricingModel;

        this.sequelizeModel.hasMany(models.PricingModel.sequelizeModel, {
            sourceKey: "id",
            foreignKey: "pricing_plan_id",
            as: "pricings",
        });
    };

    async GetAll(systemId: string): Promise<ISystemPricingPlanJson> {
        const plans = await this.sequelizeModel.findAll({
            attributes: {
                include: [
                    [Sequelize.literal("extract(epoch from date_trunc('second', pricing_plans.last_updated))"), "last_updated"],
                    ["id", "plan_id"],
                ],
                exclude: ["id"],
            },
            include: {
                model: this.pricingModel?.sequelizeModel,
                as: "pricings",
            },
            where: {
                system_id: systemId,
            },
        });

        // transform plan.pricings array to properties by pricing_type
        const results: ISystemPricingPlan[] = [];
        for (let plan of plans) {
            const pricingsToObject: { [type: string]: Array<IPricing | Partial<IPricing>> } = {};
            let { pricings, ...result } = plan.toJSON();
            for (const pricing of pricings) {
                const { pricing_plan_id, pricing_type, pricing_order, ...rest } = pricing;
                pricingsToObject[pricing_type] = [removeEmptyOrNull(rest)];
                result = { ...result, ...pricingsToObject };
            }
            results.push(result);
        }
        return {
            plans: results,
        };
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
