import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { SharedBikes } from "#sch";

/**
 * Custom Postgres model for station status vehicle type
 */
export class StationStatusVehicleTypeModel extends SequelizeModel {
    constructor() {
        super(
            SharedBikes.definitions.stationsStatusVehicleType.name + "Model",
            SharedBikes.definitions.stationsStatusVehicleType.pgTableName,
            SharedBikes.definitions.stationsStatusVehicleType.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    async GetAll(options?: any): Promise<any[]> {
        throw new Error("Method not implemented.");
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
