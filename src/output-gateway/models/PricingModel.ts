import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { SharedBikes } from "#sch";
import { IGBFSModels } from ".";

/**
 * Custom Postgres model for pricing
 */
export class PricingModel extends SequelizeModel {
    constructor() {
        super(
            SharedBikes.definitions.pricings.name + "Model",
            SharedBikes.definitions.pricings.pgTableName,
            SharedBikes.definitions.pricings.outputSequelizeAttributes,
            {
                schema: SharedBikes.pgSchema,
            }
        );
    }

    Associate = (models: IGBFSModels) => {
        this.sequelizeModel.belongsTo(models.PricingPlanModel.sequelizeModel, {
            foreignKey: "pricing_plan_id",
        });
    };

    async GetAll(options?: any): Promise<any[]> {
        throw new Error("Method not implemented.");
    }

    GetOne(id: any): Promise<any> {
        throw new Error("Method not implemented.");
    }
}
