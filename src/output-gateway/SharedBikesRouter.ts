/**
 *
 * Router /WEB LAYER/: maps routes to specific controller functions, passes request parameters and handles responses.
 * Handles web logic (http request, response). Sets response headers, handles error responses.
 */

import {
    buildGeojsonFeatureType,
    buildGeojsonFeatureCollection,
    IGeoJSONFeatureCollection,
    parseCoordinates,
    TGeoCoordinates,
} from "@golemio/core/dist/output-gateway/Geo";
import { useCacheMiddleware } from "@golemio/core/dist/output-gateway/redis";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { checkErrors, paginationLimitMiddleware, pagination } from "@golemio/core/dist/output-gateway/Validation";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { CustomError } from "@golemio/core/dist/shared/golemio-errors";
import { query, param } from "@golemio/core/dist/shared/express-validator";
import { BikeStatusModel, IBikeStatus } from "./models/BikeStatusModel";
import { models } from "./models";

export interface ISharedBikesFeature {
    geometry: TGeoCoordinates;
    properties: {
        company: {
            email?: string;
            name: string;
            phone?: string;
            web: string;
        };
        estimated_trip_length_in_km?: number | null;
        id: string;
        in_rack?: boolean;
        label: string;
        location_note?: string;
        name?: string;
        res_url: string;
        type: {
            description?: string;
            id: number;
        };
        updated_at: number;
    };
    type: string;
}

export class SharedBikesRouter extends BaseRouter {
    // Assign router to the express.Router() instance
    public router: Router = Router();
    private bikeStatusModel: BikeStatusModel;

    public constructor() {
        super();
        this.bikeStatusModel = models.BikeStatusModel;
        this.initRoutes();
    }

    private getType = (id: string): { description: string; id: number } => {
        switch (id) {
            case "bicycle":
                return { description: "bike", id: 1 };
            case "e-bike":
                return { description: "ebike", id: 2 };
            case "scooter":
                return { description: "scooter", id: 3 };
            case "padlujemecz":
                return { description: "paddleboard", id: 4 };
            default:
                return { description: "bike", id: 1 };
        }
    };

    private buildFeatureItem = (location: IBikeStatus): ISharedBikesFeature => {
        const obj = {
            id: location.id,
            in_rack: !!location.station_id,
            label: "",
            location_note: location.description,
            name: location.name,
            res_url: location.company_web,
            company: {
                name: location.company_name,
                web: location.company_web,
            },
            type: this.getType(location.type),
            updated_at: location.last_reported,
            point: location.point,
        };
        return <ISharedBikesFeature>buildGeojsonFeatureType("point", obj);
    };

    private buildFeatureCollection = (locations: IBikeStatus[]): IGeoJSONFeatureCollection => {
        const normData: ISharedBikesFeature[] = [];
        locations.forEach((location: IBikeStatus) => {
            normData.push(this.buildFeatureItem(location));
        });

        return buildGeojsonFeatureCollection(normData);
    };

    private GetAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);
            const data = await this.bikeStatusModel?.GetAll({
                lat: coords.lat as number,
                lng: coords.lng,
                range: coords.range,
                limit: Number(req.query.limit) || undefined,
                offset: Number(req.query.offset) || undefined,
                updatedSince: req.query.updatedSince as string,
                companyName: req.query.companyName as string,
            });

            res.status(200).send(this.buildFeatureCollection(data));
        } catch (err) {
            next(err);
        }
    };

    private GetOne = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await this.bikeStatusModel?.GetOne(req.params.vehicleId);
            if (!data) {
                throw new CustomError("not_found", true, "SharedBikesRouter", 404, null);
            }

            res.status(200).send(this.buildFeatureItem(data));
        } catch (err) {
            next(err);
        }
    };

    /**
     * Initiates all routes. Should respond with correct data to a HTTP requests to all routes.
     * @param {number|string} expire TTL for the caching middleware
     */
    private initRoutes = (expire?: number | string): void => {
        this.router.get(
            "/",
            [
                query("latlng").optional().isString(),
                query("range").optional().isNumeric(),
                query("limit").optional().isNumeric(),
                query("offset").optional().isNumeric(),
                query("companyName").optional().isString(),
                query("updatedSince").optional().isISO8601(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SharedBikesRouter"),
            useCacheMiddleware(expire),
            this.GetAll
        );

        this.router.get(
            "/:vehicleId",
            [param("vehicleId").exists()],
            pagination,
            checkErrors,
            paginationLimitMiddleware("SharedBikesRouter"),
            useCacheMiddleware(expire),
            this.GetOne
        );
    };
}

const sharedBikesRouter: Router = new SharedBikesRouter().router;

export { sharedBikesRouter };
