export interface IGBFSJson {
    [language: string]: {
        feeds: IGBFSJsonFeed[];
    };
}

export interface IGBFSJsonFeed {
    name: string;
    url: string | null;
}
