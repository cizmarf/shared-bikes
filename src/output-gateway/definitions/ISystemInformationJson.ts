export interface ISystemInformationJson {
    operator_id: string;
    system_id: string;
    language: string;
    logo: string;
    name: string;
    short_name: string | null;
    operator: string | null;
    url: string | null;
    purchase_url: string | null;
    start_date: string | null;
    phone_number: string | null;
    email: string | null;
    feed_contact_email: string | null;
    timezone: string;
    license_id: string | null;
    license_url: string | null;
    attribution_organization_name: string | null;
    attribution_url: string | null;
    terms_of_use_url: string | null;
    rental_apps: IRentalApp[];
}

export interface IRentalApp {
    android: {
        store_url: string | null;
        discovery_url: string | null;
    };
    ios: {
        store_url: string | null;
        discovery_url: string | null;
    };
    web_url: string | null;
}
