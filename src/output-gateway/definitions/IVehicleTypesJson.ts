export interface IVehicleTypesJson {
    vehicle_types: Array<IVehicleType | Partial<IVehicleType>>;
}

export interface IVehicleType {
    vehicle_type_id: string;
    form_factor: string | null;
    propulsion_type: string | null;
    max_range_meters: number | null;
    name: string | null;
}
