export interface ISystemPricingPlanJson {
    plans: ISystemPricingPlan[];
}

export interface ISystemPricingPlan {
    plan_id: string;
    system_id: string;
    url: string | null;
    last_updated: number;
    name: string;
    currency: string;
    price: number;
    is_taxable: boolean;
    description: string;
    surge_pricing: boolean | null;
    per_km_pricing?: IPricing[];
    per_min_pricing?: IPricing[];
    per_min_reservation_pricing?: IPricing[];
    per_min_parking_pricing?: IPricing[];
    off_station_parking?: IPricing[];
}

export interface IPricing {
    start: number;
    rate: number;
    interval: number;
    end: number | null;
    start_time_of_period: string | null;
    end_time_of_period: string | null;
}
