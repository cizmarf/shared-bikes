# Changelog

All notable changes to this project will be documented in this file.

## Unreleased

### Changed

-   New Nextbike integration

## [2.0.6] - 2022-06-07

### Changed

- change operator_id from integer to string
- Typescript version update from 4.4.4 to 4.6.4

## [2.0.5] - 2022-05-19

### Added

-   Add BeRider integration

### Changed

-   refactor: code structure and schema fixes
-   fix OG/IE SystemInformationModel

## [2.0.4] - 2022-05-18

### Fixed

-   Minor fixes in json validation schema to support strict option

## [2.0.3] - 2022-05-02

-   Add transformation of Next Bike api data to GBFS

### Changed

-   fix: remove unused rental_apps_system_info
-   fix: add pricings table primary key
-   refactor: separate RekolaStationInformationModel from generic one
-   fix: change rekola system information language to cs
-   fix: fix schemas implementation

### Added

### Changed

-   update rental app discovery url

## [2.0.2] - 2022-04-13

### Changed

-   Removed `.json` extension from GBFS endpoint urls

## [2.0.1] - 2022-04-07

### Added

-   Implement GBFS output api ([p0131#23](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/23))
-   Add OpenAPI v3.0.3 ([#4](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/4))
-   Add implementation documentation ([#6](https://gitlab.com/operator-ict/golemio/code/modules/shared-bikes/-/issues/6))

### Fixed

-   Add time zone to timestamps

## [2.0.0] - 2022-03-01

### Added

-   Implement GBFS format for Rekola trackables and geofencing zones ([p0131#22](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/22))

## Changed

-   Rewrite models from Mongo to Postgres
